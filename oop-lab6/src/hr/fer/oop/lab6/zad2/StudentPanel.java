package hr.fer.oop.lab6.zad2;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import hr.fer.oop.lab6.zad1.CourseRecord;
import hr.fer.oop.lab6.zad1.Database;
import hr.fer.oop.lab6.zad1.EnrolmentRecord;
import hr.fer.oop.lab6.zad1.StudentRecord;

public class StudentPanel extends JPanel {
	public static Database db;
	static{
		db=new Database("G:/Faks/studenti.txt","G:/Faks/predmeti.txt","G:/Faks/upisaniPredmeti.txt");
	}
	JComboBox<StudentRecord> studentsCombo;
	JTextField jmbag;
	JTextField lastName;
	JTextField firstName;
	JPanel subjectsPanel;
	
	public StudentPanel(){
		this.setBorder(BorderFactory.createTitledBorder(null,
            "", TitledBorder.LEADING,
            TitledBorder.TOP));
		setStudentsCombo();
		createInfo();
		setStudentsInfo();
		initGUI();
	}
	
	
	
	
	
	public JComboBox<StudentRecord> getStudentsCombo() {
		return studentsCombo;
	}


    /**
     * Method is used to initiate graphic interface.
     */
	private void initGUI(){
		setLayout(new BorderLayout());
		add(studentsCombo,BorderLayout.NORTH);
		
		JPanel southFramePart=new JPanel();
		southFramePart.setLayout(new BorderLayout());
		add(southFramePart, BorderLayout.CENTER);
		
		JPanel infosPanel=new JPanel();
		infosPanel.setLayout(new GridLayout(3, 2));
		southFramePart.add(infosPanel, BorderLayout.NORTH);
		
		infosPanel.add(new JLabel("JMBAG: ", SwingConstants.RIGHT));
		infosPanel.add(jmbag);
		infosPanel.add(new JLabel("Prezime: ", SwingConstants.RIGHT));
		infosPanel.add(lastName);
		infosPanel.add(new JLabel("Ime: ", SwingConstants.RIGHT));
		infosPanel.add(firstName);
		
		subjectsPanel=new JPanel();
		subjectsPanel.setLayout(new GridLayout(db.getCourseTable().getCoursesInfo().size(),1));
		subjectsPanel.setBorder(BorderFactory.createTitledBorder(null,
            "Upisani predmeti", TitledBorder.LEADING,
            TitledBorder.TOP));
		setSubjects();
		southFramePart.add(subjectsPanel,BorderLayout.CENTER);
		
		
		
		
		
	}
	/**
	 * Method is used to set all subject in GUI according to selected student.
	 */
	private void setSubjects(){
		subjectsPanel.removeAll();
		subjectsPanel.revalidate();
		subjectsPanel.repaint();
		
		Set<EnrolmentRecord> enrolmentRecords=db.getEnrolmentTable().getEnrolmentsInfo();
		Map<String, CourseRecord> courseRecords=db.getCourseTable().getCoursesInfo();
		String currentJmbag=((StudentRecord)studentsCombo.getSelectedItem()).getJmbag();
		for(EnrolmentRecord record:enrolmentRecords){
			if(currentJmbag.equals(record.getStudentJMBAG())){
				String courseName=courseRecords.get(record.getCourseID()).toString();
				JButton courseBtn=new JButton(courseName);
				courseBtn.addActionListener((event)->{
					JFrame courseInfo=new JFrame();
					JOptionPane.showMessageDialog(courseInfo, 
							"Student's grade is: "+record.getGrade(), "GRADE", JOptionPane.INFORMATION_MESSAGE);
				});
				subjectsPanel.add(courseBtn);
				
			}
		}
		
	}
	
    /**
     * Method is used to set combo button containing all students.
     */
	private void setStudentsCombo(){
		Map<String,StudentRecord> allStudentRecords=db.getStudentTable().getStudentsInfo();
		StudentRecord[] records=new StudentRecord[allStudentRecords.size()];
		int i=0;
		for(Entry<String, StudentRecord> entry:allStudentRecords.entrySet()){
			records[i]=entry.getValue();
			i++;
		}
		studentsCombo=new JComboBox<>(records);
		studentsCombo.addActionListener((event)->{
			setStudentsInfo();
			setSubjects();
		});
	}
	/**
	 * Method is used to create all needed text fields
	 */
	private void createInfo(){
		jmbag=new JTextField();
		jmbag.setEditable(false);
		lastName=new JTextField();
		lastName.setEditable(false);
		firstName=new JTextField();
		firstName.setEditable(false);
	}
	/**
	 * Method is used to set all student information
	 */
	private void setStudentsInfo(){
		jmbag.setText(((StudentRecord)studentsCombo.getSelectedItem()).getJmbag());
		lastName.setText(((StudentRecord)studentsCombo.getSelectedItem()).getLastName());
		firstName.setText(((StudentRecord)studentsCombo.getSelectedItem()).getFirstName());
		
	}
	

}
