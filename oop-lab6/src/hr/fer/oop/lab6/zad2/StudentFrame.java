package hr.fer.oop.lab6.zad2;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.UIManager.LookAndFeelInfo;
/**
 * 
 * @author Luka
 * Class is used to create frame
 */
public class StudentFrame extends JFrame {
	
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(()->{
			StudentFrame frame=new StudentFrame();
			try {
				UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
			} catch (Exception e) {
				e.printStackTrace();
			}
			frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			frame.setVisible(true);
			frame.setLocation(500,500);
			frame.setSize(500,350);
			StudentPanel studentPanel=new StudentPanel();
			frame.add(studentPanel);
	 
       
          });
}
}
