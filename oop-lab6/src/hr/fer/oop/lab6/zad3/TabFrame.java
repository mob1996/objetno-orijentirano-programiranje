package hr.fer.oop.lab6.zad3;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

import hr.fer.oop.lab6.zad2.StudentFrame;
import hr.fer.oop.lab6.zad2.StudentPanel;

public class TabFrame extends JFrame {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(()->{
			StudentFrame frame=new StudentFrame();
			try {
				UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
			} catch (Exception e) {
				e.printStackTrace();
			}
			frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			frame.setVisible(true);
			frame.setLocation(500,500);
			frame.setSize(500,350);
			JTabbedPane tab=new JTabbedPane();
			StudentPanel studentPanel=new StudentPanel();
			tab.add("Studenti", studentPanel);
			tab.add("Predmeti",new SubjectPanel(tab,studentPanel));
			frame.add(tab);
	 

          });

	}

}
