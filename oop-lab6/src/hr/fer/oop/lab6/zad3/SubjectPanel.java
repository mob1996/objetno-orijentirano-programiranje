package hr.fer.oop.lab6.zad3;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.BrokenBarrierException;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

import hr.fer.oop.lab6.zad1.CourseRecord;
import hr.fer.oop.lab6.zad1.EnrolmentRecord;
import hr.fer.oop.lab6.zad1.StudentRecord;
import hr.fer.oop.lab6.zad2.StudentPanel;

public class SubjectPanel extends JPanel {
	
	private JComboBox<CourseRecord> coursesCombo;
	private StudentPanel studentPanel; 
	private JTabbedPane tab;
	private JPanel students;

	
	
	public SubjectPanel(JTabbedPane tab, StudentPanel studentPanel){
		this.setBorder(BorderFactory.createTitledBorder(""));
		this.tab=tab;
		this.studentPanel=studentPanel;
		setCourseCombo();
		
		
		
		initGUI();
		
	}
	/**
     * Method is used to initiate graphic interface.
     */
	private void initGUI(){
		this.setLayout(new BorderLayout());
		add(coursesCombo,BorderLayout.NORTH);
		
		students=new JPanel();
		students.setLayout(new BoxLayout(students, BoxLayout.Y_AXIS));
		setStudents();
		
		
		JPanel studentsGrades=new JPanel();
		add(studentsGrades,BorderLayout.CENTER);
		studentsGrades.setLayout(new BorderLayout());
		studentsGrades.add(new JLabel(String.format("%10s%-30s%80s","", "Ime","Ocjena"), SwingConstants.LEFT),BorderLayout.NORTH);
		JPanel studentsAndDetails=new JPanel(new BorderLayout());
		studentsGrades.add(studentsAndDetails, BorderLayout.CENTER);
		studentsAndDetails.add(students,BorderLayout.NORTH);
		
	
		
		
	}
	
	/*private void setStudents(){
		students.removeAll();
		students.revalidate();
		students.repaint();
		
		
		Set<EnrolmentRecord> allEnrolments=studentPanel.db.getEnrolmentTable().getEnrolmentsInfo();
		Map<String,StudentRecord> allStudents=studentPanel.db.getStudentTable().getStudentsInfo();
		int i=0;
		String currentSubject=((CourseRecord)coursesCombo.getSelectedItem()).getCourseID();
		
		
		for(EnrolmentRecord record:allEnrolments){
			if(currentSubject.equals(record.getCourseID())){
				String name=allStudents.get(record.getStudentJMBAG()).toString();
				
				FlowLayout flow=new FlowLayout();
				flow.setAlignment(FlowLayout.RIGHT);
				JPanel textButtonPanel=new JPanel();
				textButtonPanel.setLayout(flow);
				
				JButton detalji=new JButton("Detalji");
				JTextField text=new JTextField(String.format("%-25s%80s",name,record.getGrade()));
				
				setBackground(i,text);
				text.setEditable(false);
				
				textButtonPanel.add(text);
				textButtonPanel.add(detalji);
				students.add(textButtonPanel);
				i++;
				
				detalji.addActionListener((event)->{
					StudentRecord student=allStudents.get(record.getStudentJMBAG());
					tab.setSelectedIndex(0);
					int j=0;
					for(Entry<String, StudentRecord> entry:allStudents.entrySet()){
						if(student.equals(entry.getValue())){
							studentPanel.getStudentsCombo().setSelectedIndex(j);
							break;
						}
						j++;
					}
				});
			}
		}
	}*/
	
	/**
	 * Method is used to set students names and grades.
	 */
	private void setStudents(){
		students.removeAll();
		students.revalidate();
		students.repaint();
		
		
		Set<EnrolmentRecord> allEnrolments=studentPanel.db.getEnrolmentTable().getEnrolmentsInfo();
		Map<String,StudentRecord> allStudents=studentPanel.db.getStudentTable().getStudentsInfo();
		int i=0;
		String currentSubject=((CourseRecord)coursesCombo.getSelectedItem()).getCourseID();
		
		
		for(EnrolmentRecord record:allEnrolments){
			if(currentSubject.equals(record.getCourseID())){
				String name=allStudents.get(record.getStudentJMBAG()).toString();
				
				BorderLayout bord=new BorderLayout();
				JPanel textButtonPanel=new JPanel();
				textButtonPanel.setLayout(bord);
				
				JButton detalji=new JButton("Detalji");
				JTextField text=new JTextField(String.format("%-20s%79s%s",name,"",record.getGrade()));
				
				setBackground(i,text);
				text.setEditable(false);
				
				textButtonPanel.add(text, BorderLayout.CENTER);
				textButtonPanel.add(detalji,BorderLayout.EAST);
				students.add(textButtonPanel);
				i++;
				
				detalji.addActionListener((event)->{
					StudentRecord student=allStudents.get(record.getStudentJMBAG());
					tab.setSelectedIndex(0);
					int j=0;
					for(Entry<String, StudentRecord> entry:allStudents.entrySet()){
						if(student.equals(entry.getValue())){
							studentPanel.getStudentsCombo().setSelectedIndex(j);
							break;
						}
						j++;
					}
				});
			}
		}
	}
	
	
	
	/**
	 * Method is used to set background
	 * @param num
	 * @param field
	 */
	
	private void setBackground(int num,JTextField field){
		if(num%2==0){
			field.setBackground(Color.gray);
		} else {
			field.setBackground(Color.DARK_GRAY);
		}
	}
	
	/**
	 * Method is used to create combo for subject.
	 */
	private void setCourseCombo(){
		Map<String, CourseRecord> allCoursesRecord= studentPanel.db.getCourseTable().getCoursesInfo();
		CourseRecord[] records=new CourseRecord[allCoursesRecord.size()];
		int i=0;
		for(Entry<String, CourseRecord> record:allCoursesRecord.entrySet()){
			records[i]=record.getValue();
			i++;
		}
		coursesCombo=new JComboBox(records);
		coursesCombo.addActionListener((event)->{
			setStudents();
		});
	}

}
