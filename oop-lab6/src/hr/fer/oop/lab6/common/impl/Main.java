package hr.fer.oop.lab6.common.impl;

import hr.fer.oop.lab6.common.iface.Consumer;
import hr.fer.oop.lab6.common.iface.Generator;
import hr.fer.oop.lab6.common.iface.InputMessage;
import hr.fer.oop.lab6.common.iface.OutputMessage;
import hr.fer.oop.lab6.common.iface.Processor;

public class Main {

	public static void main(String[] args) {
		Generator g = new RandomStringGenerator(10,50);
        Processor p = new MD5Digester();
        Consumer c = new SuffixCheck(new byte[] { (byte)0xFE, (byte)0xFF }, System.out);
        while(true) { 
              InputMessage msg1 = g.generateMessage();
              OutputMessage msg2 = p.process(msg1);
              msg2.getOutput().
              c.evaluate(msg2);
        }
	}

}
