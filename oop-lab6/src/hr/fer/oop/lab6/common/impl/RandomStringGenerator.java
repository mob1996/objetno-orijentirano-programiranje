package hr.fer.oop.lab6.common.impl;

import java.io.InputStream;
import java.util.Random;

import hr.fer.oop.lab6.common.iface.Generator;
import hr.fer.oop.lab6.common.iface.InputMessage;

public class RandomStringGenerator implements Generator {
	private static final char[] symbols;
	static{
		StringBuilder symbolBuilder=new StringBuilder();
		for(char cha='a';cha<='z';cha++){
			symbolBuilder.append(cha);
		}
		for(char cha='A';cha<='Z';cha++){
			symbolBuilder.append(cha);
		}
		symbols=symbolBuilder.toString().toCharArray();
	}
	
    private int minLength;
    private int maxLength;
    private int length;
    private String generatedString;
    
    public RandomStringGenerator(int minLength,int maxLength) {
		this.minLength=minLength;
		this.maxLength=maxLength;
	    generateLength();
	}
    
    private void generateLength(){
    	Random random=new Random();
    	int range=maxLength-minLength+1;
    	int fraction=(int)(range*random.nextDouble());
    	length=fraction+minLength;
    }
    
    private void generateString(){
    	char[] buf=new char[length];
    	Random random=new Random();
    	for(int index=0;index<length;index++){
    		buf[index]=symbols[random.nextInt(symbols.length)];
    	}
    	generatedString=new String(buf);
    }
    
	@Override
	public InputMessage generateMessage() {
		generateString();
		InputMessage in=new InputMessageImpl(generatedString);
		return in;
	}

}
