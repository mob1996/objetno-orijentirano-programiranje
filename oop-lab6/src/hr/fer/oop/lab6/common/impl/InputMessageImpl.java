package hr.fer.oop.lab6.common.impl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import hr.fer.oop.lab6.common.iface.InputMessage;

public class InputMessageImpl implements InputMessage {
    
	private InputStream inputStream;
	
	public InputMessageImpl(String str) {
		inputStream=new ByteArrayInputStream(str.getBytes());
	}
	@Override
	public InputStream getStream() {
		return inputStream;
	}

}
