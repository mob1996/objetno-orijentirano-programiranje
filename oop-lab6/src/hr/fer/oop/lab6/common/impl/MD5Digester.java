package hr.fer.oop.lab6.common.impl;

import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


import hr.fer.oop.lab6.common.iface.InputMessage;
import hr.fer.oop.lab6.common.iface.OutputMessage;
import hr.fer.oop.lab6.common.iface.Processor;

public class MD5Digester implements Processor {

	@Override
	public OutputMessage process(InputMessage inMessg) {
		OutputMessage out;
		try {
			MessageDigest md=MessageDigest.getInstance("MD5");
			InputStream in=inMessg.getStream();
			
			byte[] buf=new byte[1024];
			int nread=0;
			while((nread=in.read(buf))!=-1){
				md.update(buf,0,nread);
			}
			byte[] mdbytes=md.digest();
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < mdbytes.length; i++) {
		        sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
		    }
			out=new OutputMessageImpl(sb.toString(), inMessg);
			return out;
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
