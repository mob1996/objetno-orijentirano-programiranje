package hr.fer.oop.lab6.common.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import hr.fer.oop.lab6.common.iface.InputMessage;
import hr.fer.oop.lab6.common.iface.OutputMessage;

public class OutputMessageImpl implements OutputMessage {
    private InputStream outputStream;
    private InputMessage inputMessage;
    
    
    public OutputMessageImpl(String out, InputMessage inMessg) {
		this.outputStream=new ByteArrayInputStream(out.getBytes());
		this.inputMessage=inMessg;
		
	}
	@Override
	public InputStream getOutput() {
		return outputStream;
	}

	@Override
	public InputMessage getInputMessage() {
		return inputMessage;
	}

	@Override
	public void reset() {
		try {
			outputStream.reset();
			inputMessage.getStream().reset();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
