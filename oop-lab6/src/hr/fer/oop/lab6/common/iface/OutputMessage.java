package hr.fer.oop.lab6.common.iface;

import java.io.InputStream;

public interface OutputMessage {
   public InputStream getOutput();
   public InputMessage getInputMessage();
   public void reset();
}
