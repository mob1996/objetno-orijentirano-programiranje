package hr.fer.oop.lab6.common.iface;

public interface Generator {
  public InputMessage generateMessage();
}
