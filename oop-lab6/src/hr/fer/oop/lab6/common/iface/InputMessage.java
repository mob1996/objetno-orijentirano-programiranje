package hr.fer.oop.lab6.common.iface;

import java.io.InputStream;

public interface InputMessage {
   public InputStream getStream();
}
