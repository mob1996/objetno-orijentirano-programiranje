package hr.fer.oop.lab6.common.iface;

public interface Consumer {
  public void evaluate(OutputMessage outMessg);
}
