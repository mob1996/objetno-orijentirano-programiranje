package hr.fer.oop.lab6.common.iface;

public interface Processor {
  public OutputMessage process(InputMessage inMessg);
}
