package hr.fer.oop.lab6.zad1;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;



public class Demonstration {
	
	private static Database database;
	static {
		database = new Database("G:/Faks/studenti.txt", "G:/Faks/predmeti.txt",
				"G:/Faks/upisaniPredmeti.txt");
	}
	
	public static void main(String[] args) {
	       StudentTable table=database.getStudentTable();
	       for(Entry<String,StudentRecord> entry:table.getStudentsInfo().entrySet()){
	    	   System.out.println(entry.getValue());
	       }
	       System.out.println("Trazeni student:"+table.findStudentByJmbag("0251414659")+"\n\n");
	       
	       
	       CourseTable courseTable=database.getCourseTable();
	       for(Entry<String,CourseRecord> entry:courseTable.getCoursesInfo().entrySet()){
	    	   System.out.println(entry.getValue());
	       }
	       System.out.println("Trazeno prema imenu: "+courseTable.findByName("Progr*"));
	       System.out.println("Trazeno prema ID-u: "+courseTable.findByID("4")+"\n\n");
	       
	       
	       EnrolmentTable enrolmentTable=database.getEnrolmentTable();
	       for(EnrolmentRecord record:enrolmentTable.getEnrolmentsInfo()){
	    	   System.out.println(record);
	       }
	       Collection<EnrolmentRecord> enrolmentRecords;
	       enrolmentRecords=enrolmentTable.findByStudent("0251414659");
	       for(EnrolmentRecord record:enrolmentRecords){
	    	   System.out.println("Pronaden prema JMBAG-u: "+record);
	       }
	       enrolmentRecords=enrolmentTable.findByCourse("3");
	       for(EnrolmentRecord record:enrolmentRecords){
	    	   System.out.println("Pronaden prema courseID-u: "+record);
	       }
	       System.out.println("Pronaden prema JMBAG-u i courseu: "+enrolmentTable.findByStudentAndCourse("6873004907", "1"));
	      

	}

}
