package hr.fer.oop.lab6.zad1;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * This class is used to store collection of student records.
 * @author Luka
 *
 */
public class StudentTable {
   private Map<String,StudentRecord> studentsInfo;
   
   public StudentTable(List<String> students){
	   studentsInfo=new HashMap<>();
	   for(String student:students){
		   String[] line=student.split("\t");
		   studentsInfo.put(line[0], new StudentRecord(line[0],line[1],line[2],Integer.parseInt(line[3])));
	   }
   }
   
   /**
    * Method is used to find student by his jmbag.
    * @param jmbag
    * @return
    */
   public StudentRecord findStudentByJmbag(String jmbag){
	   StudentRecord student;
	   student=studentsInfo.get(jmbag);
	   try{
		   if(student==null)
			   throw new NullPointerException("Student "+jmbag+" has not been found.");
	   } catch(NullPointerException exc){
		   System.out.println(exc);
	   }
	   
	   return student;
   }


   public Map<String, StudentRecord> getStudentsInfo() {
	   return studentsInfo;
   }
   
   
   
   
   
}
