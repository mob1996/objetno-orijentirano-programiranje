package hr.fer.oop.lab6.zad1;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
/**
 * 
 * @author Luka
 * This class is used to store collection of course records
 */
public class CourseTable {
    private Map<String,CourseRecord> coursesInfo;
  
    public CourseTable(List<String> courses){
    	coursesInfo=new HashMap<>();
    	for(String course:courses){
    		String[] line=course.split("\t");
    		coursesInfo.put(line[0], new CourseRecord(line[0],line[1]));
    	}
    }
    
    /**
     * Method is used to find and return courseRecord by its ID.
     * @param courseID
     * @return
     */
    public CourseRecord findByID(String courseID){
    	CourseRecord course;
    	course=coursesInfo.get(courseID);
    	try{
 		   if(course==null)
 			   throw new NullPointerException("Course with ID "+courseID+" has not been found.");
 	   } catch(NullPointerException exc){
 		   System.out.println(exc);
 	   }
    	return course;
    }
    
    /**
     * This method is used to find and return CourseRecord by its name.
     * @param partialCourseName
     * @return
     */
    public CourseRecord findByName(String partialCourseName){
    	CourseRecord course=null;
    	String searchingName=partialCourseName.replace("*", "").toLowerCase();
    	for(Entry<String,CourseRecord> entry:coursesInfo.entrySet()){
    		String courseName=entry.getValue().getCourseName().toLowerCase();
    		if(courseName.contains(searchingName)){
    			course=entry.getValue();
    			break;
    		}
    	}
    	
    	try{
 		   if(course==null)
 			   throw new NullPointerException("Course with that name doesn't exist.");
 	   } catch(NullPointerException exc){
 		   System.out.println(exc);
 	   }
    	return course;
    }

	public Map<String, CourseRecord> getCoursesInfo() {
		return coursesInfo;
	}
    
    
    
    
    
}
