package hr.fer.oop.lab6.zad1;
/**
 * 
 * @author Luka
 * This class is used to create instance of one course.
 */
public class CourseRecord {
	private String courseID;
	private String courseName;
	
	public CourseRecord(String courseID, String courseName){
		this.courseID=courseID;
		this.courseName=courseName;
	}

	public String getCourseID() {
		return courseID;
	}

	public void setCourseID(String courseID) {
		this.courseID = courseID;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((courseID == null) ? 0 : courseID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CourseRecord other = (CourseRecord) obj;
		if (courseID == null) {
			if (other.courseID != null)
				return false;
		} else if (!courseID.equals(other.courseID))
			return false;
		return true;
	}
	
	@Override 
	public String toString(){
		return String.format("%s",courseName);
	}
	
	

}
