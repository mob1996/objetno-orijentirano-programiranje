package hr.fer.oop.lab6.zad1;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
/**
 * 
 * @author Luka
 * This class is used to unify all information about students
 * courses and enrolments.
 */
public class Database {
	private StudentTable studentTable;
	private CourseTable courseTable;
	private EnrolmentTable enrolmentTable;
	
	public Database(String student, String course, String enrolment){
		studentTable=new StudentTable(reader(student));
		courseTable=new CourseTable(reader(course));
		enrolmentTable=new EnrolmentTable(reader(enrolment));
	}
	
	/**
	 * Method is used to read from files 
	 * @param file
	 * @return
	 */
	public List<String> reader(String file){
		List<String> lines=new LinkedList<>();
		try(BufferedReader br= new BufferedReader(
					new InputStreamReader(
				    new BufferedInputStream(
				    new FileInputStream(file)), "UTF-8")))
		{
			String line;
			while((line=br.readLine())!=null){
				lines.add(line);
			}
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			System.out.println("File "+file+" doesn't exist.");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		return lines;
	}

	public StudentTable getStudentTable() {
		return studentTable;
	}

	public CourseTable getCourseTable() {
		return courseTable;
	}

	public EnrolmentTable getEnrolmentTable() {
		return enrolmentTable;
	}
	
	

}
