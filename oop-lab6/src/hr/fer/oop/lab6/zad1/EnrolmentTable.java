package hr.fer.oop.lab6.zad1;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
/*
 * This class is used to store collection of enrolment records
 */
public class EnrolmentTable {
	private Set<EnrolmentRecord> enrolmentsInfo;
	
	public EnrolmentTable(List<String> enrolements){
		enrolmentsInfo=new HashSet<>();
		for(String enrolement:enrolements){
			String[] line=enrolement.split("\t");
			enrolmentsInfo.add(new EnrolmentRecord(line[0],line[1], Integer.parseInt(line[2])));
		}
	}
	
	
	
	public Set<EnrolmentRecord> getEnrolmentsInfo() {
		return enrolmentsInfo;
	}


    /**
     * Method is used to find all courses that student took by his jmbag
     * @param studentJMBAG
     * @return
     */
	public Collection<EnrolmentRecord> findByStudent(String studentJMBAG){
		Collection<EnrolmentRecord> studentEnrolments=new HashSet<>();
		for(EnrolmentRecord enrolment:enrolmentsInfo){
			if(studentJMBAG.equals(enrolment.getStudentJMBAG())){
				studentEnrolments.add(enrolment);
			}
		}
		
		return studentEnrolments;
	}
	/**
	 * Method is used to find all students that took course.
	 * @param courseID
	 * @return
	 */
	public Collection<EnrolmentRecord> findByCourse(String courseID){
		Collection<EnrolmentRecord> courseEnrolments=new HashSet<>();
		for(EnrolmentRecord enrolment:enrolmentsInfo){
			if(courseID.equals(enrolment.getCourseID())){
				courseEnrolments.add(enrolment);
			}
		}
		return courseEnrolments;
	}
	/**
	 * Method is used to find student that took given course.
	 * @param studentJMBAG
	 * @param courseID
	 * @return
	 */
	public EnrolmentRecord findByStudentAndCourse(String studentJMBAG, String courseID){
		EnrolmentRecord record=null;
		for(EnrolmentRecord enrolment:enrolmentsInfo){
			if(studentJMBAG.equals(enrolment.getStudentJMBAG()) && 
			   courseID.equals(enrolment.getCourseID())){
				  record=enrolment;
				  break;
			   }
		}
			
	    try{
		   if(record==null)
		   throw new NullPointerException("Enrolment record doesn't exist.");
	    } catch(NullPointerException exc){
		   System.out.println(exc);
	    }
		return record;
	}
	
	public EnrolmentRecord newCourse(String studentJMBAG, String courseID){
		EnrolmentRecord newRecord=new EnrolmentRecord(courseID, studentJMBAG,0);
		return newRecord;
	}
	/**
	 * Method is used to add another enrolment.
	 * @param record
	 */
	public void updateEnrolment(EnrolmentRecord record){
		if(enrolmentsInfo.contains(record)){
			enrolmentsInfo.remove(record);
			enrolmentsInfo.add(record);
		} else {
			enrolmentsInfo.add(record);
		}
	}
	/**
	 * Method is used to remove enrolment.
	 * @param studentJMBAG
	 * @param courseID
	 */
	public void deleteRecord(String studentJMBAG, String courseID){
		EnrolmentRecord record=newCourse(studentJMBAG,courseID);
	    if(enrolmentsInfo.remove(record)){
	    	System.out.println("Enrolement record has been deleted.");
	    } else {
	    	System.out.println("Enrolement record doesn't exist.");
	    }
		
	}
	
	

}
