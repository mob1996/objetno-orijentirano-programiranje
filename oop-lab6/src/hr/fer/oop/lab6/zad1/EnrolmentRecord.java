package hr.fer.oop.lab6.zad1;
/**
 * 
 * @author Luka
 * This class is used for creating connection between one student
 * and one course
 */
public class EnrolmentRecord {
   private String courseID;
   private String studentJMBAG;
   private int grade;
   
   public EnrolmentRecord(String courseID, String studentJMBAG, int grade){
	   this.courseID=courseID;
	   this.studentJMBAG=studentJMBAG;
	   this.grade=grade;
   }

   public String getCourseID() {
	   return courseID;
   }

   public void setCourseID(String courseID) {
       this.courseID = courseID;
   }

   public String getStudentJMBAG() {
 	   return studentJMBAG;
   }

   public void setStudentJMBAG(String studentJMBAG) {
	   this.studentJMBAG = studentJMBAG;
   }

   public int getGrade() {
	   return grade;
   }

   public void setGrade(int grade) {
	   this.grade = grade;
   }

   @Override
   public int hashCode() {
	   final int prime = 31;
	   int result = 1;
	   result = prime * result + ((courseID == null) ? 0 : courseID.hashCode());
	   result = prime * result + ((studentJMBAG == null) ? 0 : studentJMBAG.hashCode());
	   return result;
   }

   @Override
   public boolean equals(Object obj) {
	   if (this == obj)
		   return true;
	   if (obj == null)
		   return false;
	   if (getClass() != obj.getClass())
		   return false;
	   EnrolmentRecord other = (EnrolmentRecord) obj;
	   if (courseID == null) {
		   if (other.courseID != null)
			   return false;
	   } else if (!courseID.equals(other.courseID))
		   return false;
	   if (studentJMBAG == null) {
		   if (other.studentJMBAG != null)
			   return false;
	   } else if (!studentJMBAG.equals(other.studentJMBAG))
		   return false;
	   return true;
   }
   
   @Override
   public String toString(){
	   return String.format("%s %s %s", courseID, studentJMBAG, grade);
   }
   
   
   
   
}
