package hr.fer.opp.lab4.prob3;

import java.util.function.Predicate;

public interface IManager {
	/**
	 * Method is used to place offeredPlayers that satisfy criteria in registered player list.
	 * @param offeredPlayers
	 * @param criteria
	 * @throws UnemployedCoachException if managingTeam==null
	 */
	public void registerPlayers(Iterable<FootballPlayer> offeredPlayers, Predicate<FootballPlayer> criteria) throws UnemployedCoachException;
	/**
	 * Method is used to place players from registered player list that satisfy given criteria into starting eleven.
	 * @param criteria
	 * @throws UnemployedCoachException if managingTeam==null
	 */
	public void pickStartingEleven(Predicate<FootballPlayer> criteria) throws UnemployedCoachException;
	/**
	 * Used when we want to change team's formation to Coach's favorite formation
	 * @throws UnemployedCoachException if managingTeam==null
	 */
	public void forceMyFormation() throws UnemployedCoachException;
	/**
	 * Sets managingTeam to team
	 * @param team
	 */
	public void setManagingTeam(IManegableTeam team);

}
