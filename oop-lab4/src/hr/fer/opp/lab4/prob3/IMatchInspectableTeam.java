package hr.fer.opp.lab4.prob3;

public interface IMatchInspectableTeam {
	/**
	 * Checks if starting eleven has seven or more players
	 * @return true if it does, false if it doesn't
	 */
	public boolean isMatchReady();
	
	/**
	 * Calculates team spirit 
	 * @return teamSpirit
	 */
	public int calculateTeamSpirit();
	/**
	 * Calculates team skill
	 * @return teamSkill
	 */
	public int calculateTeamSkill();
	
	/**
	 * Calculates team rating
	 * @return teamRating
	 */
	public double calculateRating();
	

}
