package hr.fer.opp.lab4.prob3;
/**
 * 
 * @author Luka
 * Enumated class that contains 4 playing positions
 */
public enum PlayingPosition {
      GK, DF, MF, FW;
}
