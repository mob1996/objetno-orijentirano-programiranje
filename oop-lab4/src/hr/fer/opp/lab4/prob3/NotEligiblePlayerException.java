package hr.fer.opp.lab4.prob3;
/**
 * 
 * @author Luka
 * Exception that is used when working with class FootballPlayer
 *
 */
public class NotEligiblePlayerException extends RuntimeException{
	
	public NotEligiblePlayerException(){
		
	}
	
	public NotEligiblePlayerException(String message){
		super(message);
	}
	
	public NotEligiblePlayerException(Throwable cause){
		super(cause);
	}
	
	public NotEligiblePlayerException(String message, Throwable cause){
		super(message, cause);
	}
	

}
