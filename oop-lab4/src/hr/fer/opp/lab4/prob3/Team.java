package hr.fer.opp.lab4.prob3;
import java.util.*;
import java.util.function.Predicate;
/**
 * 
 * @author Luka
 * Abstract method Team is used as a base for creating
 * instances of ClubTeam and NationalTeam.
 *
 */
public abstract class Team implements IManegableTeam, IMatchInspectableTeam{
   private static final int MAX_STARTING_ELEVEN=11;
   private static final int NEEDED_PLAYERS_FOR_READY=7;
   private String name;
   private Formation teamFormation;
   protected LinkedHashSet<FootballPlayer> registratedPlayers;
   protected LinkedHashSet<FootballPlayer> startingEleven;
   /**
    * Public constructor for class Team.
    * @param name Name of the Team we want to create.
    * @param teamFormation Formation of the team we want to create.
    */
   public Team(String name, Formation teamFormation){
	   if(name==null){
		   throw new IllegalArgumentException("Team name can not be null!");
	   }
	   this.name=name;
	   if(teamFormation==null){
		   throw new IllegalArgumentException("Formation can not be null!");
	   }
	   this.teamFormation=teamFormation;
	   registratedPlayers=new LinkedHashSet<>();
	   startingEleven=new LinkedHashSet<>();
   }

   public Formation getTeamFormation() {
	   return teamFormation;
   }

   public void setTeamFormation(Formation teamFormation) {
	   this.teamFormation = teamFormation;
   }

   public String getName() {
	   return name;
   }
   
   
   public LinkedHashSet<FootballPlayer> getRegistratedPlayers() {
	   return registratedPlayers;
   }

   public LinkedHashSet<FootballPlayer> getStartingEleven() {
	return startingEleven;
   }
   
   /**
    * Method is abstract because every class that extends Team has
    * different implementation.
    */
   @Override
   public abstract void registerPlayer(FootballPlayer player);
   
   /**
    * Method is abstract because every class that extends Team has
    * different implementation.
    */
   @Override
   public abstract boolean isPlayerRegistrable(FootballPlayer player);
   
   /**
    * Method is used for removing player from registratedPlayer list
    */
   @Override
   public void unregisterPlayer(FootballPlayer player){
	   if(!(registratedPlayers.contains(player))){
		   throw new IllegalArgumentException("Player "+player.getName()+" is not on the registrated player list,"+
	                                           " making him impossible to remove");
	   }
	   registratedPlayers.remove(player);
   }
   /**
    * Method is used for adding players from registratedPlayer list to startingEleven
    */
   @Override
   public void addPlayerToStartingEleven(FootballPlayer player){
	   if(!registratedPlayers.contains(player)){
		   throw new NotEligiblePlayerException("Player "+player.getName()+" is not registrated, so he cant be added to starting eleven!");
	   }
	   if(startingEleven.size()>=MAX_STARTING_ELEVEN ){
		   throw new NotEligiblePlayerException("Player "+player.getName()+" can not be added to starting eleven because it is full");
	   }
	   if(!(startingEleven.add(player))){
		   throw new NotEligiblePlayerException("Player "+player.getName()+" is already on the list");
	   }
   }
   /**
    * Method is used for removing player from startingEleven
    */
   @Override
   public void removePlayerFromStartingEleven(FootballPlayer player){
	   if(!(startingEleven.contains(player))){
		   throw new IllegalArgumentException("Player "+player.getName()+" is not on the starting eleven list,"+
                                              " making him impossible to remove");
	   }
	   startingEleven.remove(player);
   }
   /**
    * Method removes all elements from startingElevn
    */
   @Override
   public void clearStartingEleven(){
	   startingEleven.clear();
   }
   
   /**
    * Method is used for filtering and placing players into newCollection from registratedPlayers
    */
   @Override
   public Collection<FootballPlayer> filterRegisteredPlayers(Predicate<FootballPlayer> criteria){
	   Collection<FootballPlayer> newCollection=new LinkedHashSet<>();
	   for(FootballPlayer player:registratedPlayers){
		   if(criteria.test(player)){
			   newCollection.add(player);
		   }
	   }
	   return newCollection;
   }
   /**
    * Method is used for calculation overall teamSkill
    */
   @Override
   public int calculateTeamSkill(){
	   int teamSkill=0;
	   for(FootballPlayer player:startingEleven){
		   teamSkill+=player.getPlayingSkill();
	   }
	   return teamSkill;
   }
   /**
    * Method is used for callculating overall teamSpirit
    */
   @Override
   public int calculateTeamSpirit(){
	   int teamSpirit=0;
	   for(FootballPlayer player:startingEleven){
		   teamSpirit+=player.getEmotion();
	   }
	   return teamSpirit;
   }
   /**
    * Method is used for checking if team is ready for playing.
    */
   @Override
   public boolean isMatchReady(){
	   if(startingEleven.size()>=NEEDED_PLAYERS_FOR_READY){
		   return true;
	   }
	   return false;
   }
  
   @Override
   public abstract double calculateRating();
   
   
   
   
   
}
