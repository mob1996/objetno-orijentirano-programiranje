package hr.fer.opp.lab4.prob3;
import java.util.*;
import java.util.function.Predicate;
public interface IManegableTeam {
   /**
    * Adds player on registrated player list
    * @param player Instance of the FootballPlayer
    * @throws NotEligiblePlayerException this exception is thrown when player is already on the list,
    *         if list is full or when some other criteria is not satisfied
    */
   public void registerPlayer(FootballPlayer player) throws NotEligiblePlayerException;
   
   /**
    * Method is used to check if player satisfies criteria or if list has enough space for more players
    * @param player
    * @return
    */
   public boolean isPlayerRegistrable(FootballPlayer player);
   
   /**
    * Removes player from registrated player list
    * @param player Instance of the FootballPlayer
    * @throws IllegalArgumentException is thrown when player is not on the list
    */
   public void unregisterPlayer(FootballPlayer player) throws IllegalArgumentException;
   
   /**
    * Removes all players from starting eleven
    */
   public void clearStartingEleven();
   /**
    * Adds player to the starting eleven
    * @param player instance of the FootballPlayer
    * @throws NotEligiblePlayerException is thrown when player is not registrated.
    */
   public void addPlayerToStartingEleven(FootballPlayer player) throws NotEligiblePlayerException;
   
   /**
    * Removes player from the starting eleven
    * @param player Instance of the FootballPlayer
    * @throws IllegalArgumentException is thrown when player is not on the list
    */
   public void removePlayerFromStartingEleven(FootballPlayer player) throws IllegalArgumentException;
  
   public void setTeamFormation(Formation formation);
   public Formation getTeamFormation();
   
   /**
    * Creates new list that is filled by players from registrated players who satisfy given criteria.
    * @param criteria
    * @return
    */
   public Collection<FootballPlayer> filterRegisteredPlayers(Predicate<FootballPlayer> criteria);
  
}
