package hr.fer.opp.lab4.prob3;

public class UnemployedCoachException extends RuntimeException {
    public UnemployedCoachException(){
		
	}
	
	public UnemployedCoachException(String message){
		super(message);
	}
	
	public UnemployedCoachException(Throwable cause){
		super(cause);
	}
	
	public UnemployedCoachException(String message, Throwable cause){
		super(message, cause);
	}
}
