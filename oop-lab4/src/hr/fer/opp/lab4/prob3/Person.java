package hr.fer.opp.lab4.prob3;
/**
 * 
 * @author Luka Mijic
 * Abstract Class Person is used as a base for creating 2 other classes
 * called FootballPlayer and Coach.
 *
 */
public abstract class Person {
    private String name;
    private String country;
    private int emotion;
    
    
    /**
     * Constructor for class person   
     * @param name name of the person
     * @param country country of origin
     * @param emotion emotion showed by integer number
     */ 
    public Person(String name, String country, int emotion){
    	if(name==null){
    		throw new IllegalArgumentException("Name can not be null!");
    	}
    	this.name=name;
    	if(country==null){
    		throw new IllegalArgumentException("Country can not be null!");
    	}
        this.country=country;
    	if(emotion<0 || emotion>100){
    	    throw new IllegalArgumentException("Emotion has to be above 0 and above 100");
    	}
    	this.emotion=emotion;
    }


	public String getName() {
		return name;
	}


	public String getCountry() {
		return country;
	}


	public int getEmotion() {
		return emotion;
	}
	
	/**
	 * Method is used for checking if 2 objects have same hashCode(), 
	 * If they do it returns true, otherwise false
	 */
	@Override
	public boolean equals(Object obj){
		if(!(obj instanceof Person)){
			return false;
		}
		if(this.hashCode()==obj.hashCode()){
			return true;
		}
		return false;
	}
	/**
	 * If we didnt override method hashCode, method 
	 * equals wouldnt work with class Persona and classes that
	 * extend it.
	 */
	@Override
	public int hashCode(){
		int hash=37;
		hash=31*hash+(name==null ? 0:name.hashCode());
		hash=31*hash+(country==null ? 0:country.hashCode());
		hash=31*hash+emotion;
		return hash;
	}
       
       
}
