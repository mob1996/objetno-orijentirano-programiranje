package hr.fer.opp.lab4.prob4;

public class UnemployedCoachException extends Exception {
    public UnemployedCoachException(){
		
	}
	
	public UnemployedCoachException(String message){
		super(message);
	}
	
	public UnemployedCoachException(Throwable cause){
		super(cause);
	}
	
	public UnemployedCoachException(String message, Throwable cause){
		super(message, cause);
	}
}
