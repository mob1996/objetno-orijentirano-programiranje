package hr.fer.opp.lab4.prob4;

public interface IPlayableMatch {
	/**
	 * Starts match
	 * @throws NotPlayableMatchException 
	 */
	public void play() throws NotPlayableMatchException;

}
