package hr.fer.opp.lab4.prob4;

public enum MatchOutcome {
   NOT_AVAILABLE, HOME_WIN, DRAW, AWAY_WIN;
}
