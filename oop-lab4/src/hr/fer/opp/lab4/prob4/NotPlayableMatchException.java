package hr.fer.opp.lab4.prob4;

public class NotPlayableMatchException extends Exception {
    public NotPlayableMatchException(){
		
	}
	
	public NotPlayableMatchException(String message){
		super(message);
	}
	
	public NotPlayableMatchException(Throwable cause){
		super(cause);
	}
	
	public NotPlayableMatchException(String message, Throwable cause){
		super(message, cause);
	}
}
