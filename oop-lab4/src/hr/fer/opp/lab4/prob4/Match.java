package hr.fer.opp.lab4.prob4;

public class Match implements IPlayableMatch {
   private IMatchInspectableTeam home;
   private IMatchInspectableTeam away;
   private MatchType type;
   private MatchOutcome outcome;
   /**
    * Public constructor for creating instance of class Match
    * @param home represents home team
    * @param away represents away team
    * @param type represents type of the match
    */
   public Match(IMatchInspectableTeam home, IMatchInspectableTeam away,
		        MatchType type) {
	   this.home=home;
	   this.away=away;
	   this.type=type;
	   this.outcome=MatchOutcome.NOT_AVAILABLE;
   }

   public IMatchInspectableTeam getHome() {
	   return home;
   }

   public IMatchInspectableTeam getAway() {
       return away;
   }

   public MatchType getType() {
	   return type;
   }

   public MatchOutcome getOutcome() {
	   return outcome;
   }
   /**
    * Method is used to simulate football match
    */
   public void play() throws NotPlayableMatchException{
	   if(home==null){
		   throw new NotPlayableMatchException("Match can't be played because there is no home team!");
	   } else if(away==null){
		   throw new NotPlayableMatchException("Match can't be played because there is no away team!");
	   } else if(type==MatchType.COMPETITIVE){
		   if((home instanceof ClubTeam && away instanceof NationalTeam) || (home instanceof NationalTeam && away instanceof ClubTeam)){
			   throw new NotPlayableMatchException("Competitive match can't be played between club team and national team!");
		   }
	   }else if(!home.isMatchReady()){
		   throw new NotPlayableMatchException("Home team is not ready!");
	   } else if(!away.isMatchReady()){
		   throw new NotPlayableMatchException("Away team is not ready!");
	   }
	   
	   double h=home.calculateRating()/(home.calculateRating()+away.calculateRating());
	   double a=away.calculateRating()/(home.calculateRating()+away.calculateRating());
	   double min=Math.min(h, a);
	   double randomNumber=Math.random();
	   if(randomNumber<h-min/2){
		   outcome=MatchOutcome.HOME_WIN;
	   } else if(randomNumber>h+min/2){
		   outcome=MatchOutcome.AWAY_WIN;
	   } else {
		   outcome=MatchOutcome.DRAW;
	   }
	   
   }
   
   
   
   
}
