package hr.fer.opp.lab4.prob4;

import java.util.Collection;
import java.util.function.Predicate;

/**
 * 
 * @author Luka
 * Class that contains ever needed information for football coach
 *
 */
public class Coach extends Person implements IManager {
	private final static int NUM_OF_PLAYING_POSITIONS=4;
	private int coachingSkill;
	private Formation favoriteFormation;
	IManegableTeam managingTeam;
	/**
	 * Only constructor that can be used for creating instance of class Coach
	 * @param name Name of the Coach
	 * @param country  Coach's country of origin
	 * @param emotion  Coach's emotion on scale 0-100
	 * @param coachingSkill Coach's coaching skill on scale 0-100
	 * @param favoriteFormation Coach's favorite formation
	 */
	public Coach(String name, String country, int emotion, int coachingSkill, Formation favoriteFormation){
		super(name,country, emotion);
		if(coachingSkill>100 || coachingSkill<0){
			throw new IllegalArgumentException("coachingSkill must be above 0 and below 100");
		}
		this.coachingSkill=coachingSkill;
		if(favoriteFormation==null){
			throw new IllegalArgumentException("favoriteFormation can not be null");
		}
		this.favoriteFormation=favoriteFormation;
	}

	public int getCoachingSkill() {
		return coachingSkill;
	}

	public void setCoachingSkill(int coachingSkill) {
		this.coachingSkill = coachingSkill;
	}

	public Formation getFavoriteFormation() {
		return favoriteFormation;
	}

	public void setFavoriteFormation(Formation favoriteFormation) {
		this.favoriteFormation = favoriteFormation;
	}
	/**
	 * Method is used to place players from offeredPlayers to registered player list 
	 * if they satisfy given criteria.
	 */
	public void registerPlayers(Iterable<FootballPlayer> offeredPlayers, Predicate<FootballPlayer> criteria) throws UnemployedCoachException{
		if(managingTeam==null){
			throw  new UnemployedCoachException("Coach "+getName()+" is unemployed!");
		}
		for(FootballPlayer player:offeredPlayers){
			if(criteria.test(player)){
				if(!managingTeam.isPlayerRegistrable(player)){
					continue;
				} else{
					try{
						managingTeam.registerPlayer(player);
					} catch(NotEligiblePlayerException exc){
						System.out.println(exc);
					}
				}
			}
		}
	}
	/**
	 * Method is used to place players from registered player list to starting eleven 
	 * if they satisfy given criteria and if it is acceptable by team formation.
	 */
	public void pickStartingEleven(Predicate<FootballPlayer> criteria) throws UnemployedCoachException{
		if(managingTeam==null){
			throw  new UnemployedCoachException("Coach "+getName()+" is unemployed!");
		}
		managingTeam.clearStartingEleven();
		Integer[] playersPerPosition=new Integer[NUM_OF_PLAYING_POSITIONS];
		Collection<FootballPlayer> passablePlayers=managingTeam.filterRegisteredPlayers(criteria);
		for(int i=0, n=favoriteFormation.toString().length();i<n;i++){
		    int transformator=managingTeam.getTeamFormation().toString().charAt(i);
		    if(transformator<='0' || transformator>'9'){
		    	playersPerPosition[i]=1;
		    } else{
		    	playersPerPosition[i]=transformator-'0';
		    }
		}
		
		for(FootballPlayer player:passablePlayers){
		    if(player.getPlayingPosition()==PlayingPosition.GK){
		    	if(playersPerPosition[0]==0){
		    		continue;
		    	} else{
		    		playersPerPosition[0]--;
		    		try{
		    		    managingTeam.addPlayerToStartingEleven(player);
		    		}catch(NotEligiblePlayerException exc){
		    			System.out.println(exc);
		    		}
		    	}
		   }else if(player.getPlayingPosition()==PlayingPosition.DF){
			   if(playersPerPosition[1]==0){
				   continue;
			   } else{
				   playersPerPosition[1]--;
				   try{
			    		managingTeam.addPlayerToStartingEleven(player);
			    	}catch(NotEligiblePlayerException exc){
			    		System.out.println(exc);
			    	}
			   }
		   } else if(player.getPlayingPosition()==PlayingPosition.MF){
			   if(playersPerPosition[2]==0){
				   continue;
			   } else{
				   playersPerPosition[2]--;
				   try{
			    	     managingTeam.addPlayerToStartingEleven(player);
			    	}catch(NotEligiblePlayerException exc){
			    	     System.out.println(exc);
			    	}
		       }
		   } else if(player.getPlayingPosition()==PlayingPosition.FW){
			   if(playersPerPosition[3]==0){
				   continue;
			   } else{
				   playersPerPosition[3]--;
				   try{
			    		managingTeam.addPlayerToStartingEleven(player);
			    	}catch(NotEligiblePlayerException exc){
			    		System.out.println(exc);
			    	}
		       }
		   }
		 }
		}
	/**
	 * Coach's formation overthrows manegableTeam's formation
	 */
	public void forceMyFormation() throws UnemployedCoachException{
		if(managingTeam==null){
			throw  new UnemployedCoachException("Coach "+getName()+" is unemployed!");
		}
		managingTeam.setTeamFormation(favoriteFormation);	
	}
	/**
	 * Gives coach team that he can manage.
	 */
	public void setManagingTeam(IManegableTeam team){
		this.managingTeam=team;
	}
	
	/**
	 * Method used for turning class coach in writable string
	 */
	@Override
	public String toString(){
		return "(Country: "+getCountry()+"- "+getName()+"- Skill: "+ coachingSkill+"- Favorite formation: "
				+ favoriteFormation+"- Emotion: "+getEmotion()+")";
	}
	
	
	

}
