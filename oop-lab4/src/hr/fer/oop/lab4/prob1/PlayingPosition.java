package hr.fer.oop.lab4.prob1;
/**
 * 
 * @author Luka
 * Enumated class that contains 4 playing positions
 */
public enum PlayingPosition {
       FW,MF,DF, GK;
}
