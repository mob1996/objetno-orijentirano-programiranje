package hr.fer.oop.lab4.prob1;
import java.util.*;
/**
 * 
 * @author Luka
 * Class containing main method for demonstrating that program works.
 */
public class Demonstration {

	public static void main(String[] args) {
		FootballPlayer[] players=new FootballPlayer[6];
		HashSet<FootballPlayer> igraci=new LinkedHashSet<>();
		try{
		players[0]=new FootballPlayer("Ronaldo","Brazil",-5,95,PlayingPosition.FW);
		} catch (IllegalArgumentException ex){
			System.out.println("Greska: " +ex+"\nOporavak od pogreske...");
			players[0]=new FootballPlayer("Ronaldo","Brazil",93,95,PlayingPosition.FW);
		}
		
		try{
		players[1]=new FootballPlayer("Modric","Hrvatska",80,-3,PlayingPosition.MF);
		} catch(IllegalArgumentException ex){
			System.out.println("Greska: " +ex+"\nOporavak od pogreske...");
			players[1]=new FootballPlayer("Modric","Hrvatska",80,85,PlayingPosition.MF);
		}
		
		try{
		players[2]=new FootballPlayer(null,"Hrvatska",80,87,PlayingPosition.MF);
		} catch(IllegalArgumentException ex){
			System.out.println("Greska: " +ex+"\nOporavak od pogreske...");
			players[2]=new FootballPlayer("Modric","Hrvatska",80,87,PlayingPosition.MF);
		}
		
		try{
		players[3]=new FootballPlayer("Messi",null,90,87,PlayingPosition.FW);
		} catch(IllegalArgumentException ex){
			System.out.println("Greska: " +ex+"\nOporavak od pogreske...");
			players[3]=new FootballPlayer("Messi","Argentina",90,87,PlayingPosition.FW);
		}
		
		try{
		players[4]=new FootballPlayer("Van der Saar","Nizozemska",90,90,null);
		} catch(IllegalArgumentException ex){
			System.out.println("Greska: " +ex+"\nOporavak od pogreske...");
			players[4]=new FootballPlayer("Van der Saar","Nizozemska",90,90,PlayingPosition.GK);
		}
		
		players[5]=new FootballPlayer("Van der Saar","Nizozemska",90,90,PlayingPosition.GK);
		
		
		for(FootballPlayer p:players){
			System.out.println(igraci.add(p)+" HashCode: "+p.hashCode());
		}
		
		
		for(FootballPlayer p:igraci){
			System.out.println(p);
		}
		
		
		FootballPlayer player=new FootballPlayer("Ronaldo","Brazil",85,95,PlayingPosition.FW);

        Coach[] coaches=new Coach[2];
        try{
        coaches[0]=new Coach("Jurcic", "Hrvatska", 40, -10,Formation.F442);
        } catch(IllegalArgumentException ex){
        	System.out.println("Greska: " +ex+"\nOporavak od pogreske...");
        	coaches[0]=new Coach("Jurcic", "Hrvatska", 40, 50,Formation.F442);
        }
        try{
        coaches[1]=new Coach("Modric","Hrvatska",80,87,null);
        } catch(IllegalArgumentException ex){
        	System.out.println("Greska: " +ex+"\nOporavak od pogreske...");
        	coaches[1]=new Coach("Modric","Hrvatska",80,87,Formation.F541);
        }
        System.out.println(coaches[1].equals(players[1]));
       
        
	}

}
