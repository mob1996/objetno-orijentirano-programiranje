package hr.fer.oop.lab4.prob2;
/**
 * 
 * @author Luka
 * Class that defines National Football Team,
 * it extends class Team.
 *
 */
public class NationalTeam extends Team {
	public static final int MAX_REGISTRATED_PLAYERS=23;
	private String country;
	
    /**
     * Public constructor used for creating instance of National team
     * @param name Name of the team
     * @param teamFormation Team's formation
     * @param country Home country of national team
     */
	public NationalTeam(String name, Formation teamFormation, String country){
		super(name,teamFormation);
		if(country==null){
			throw new IllegalArgumentException("Country's name can not be null!");
		}
		this.country=country;
	}
	
	/**
	 * Method is used for adding players to registered players list.
	 */
	@Override
	public void registerPlayer(FootballPlayer player) {
		if(!isPlayerRegistrable(player)){
			throw new NotEligiblePlayerException("Player "+player.getName()+"can not be added to registrated player "+
		                                         "list because it is already full or his country doesn't match team's");
		}
			
	    if(!registratedPlayers.add(player)){
		    throw new NotEligiblePlayerException("Player "+player.getName()+" is already in the team");
	    }		
	}
	
    /**
     * Method is used for checking if player can be added to registeredPlayers list
     */
	
	@Override
	public boolean isPlayerRegistrable(FootballPlayer player) {
		if(!player.getCountry().equals(country) || registratedPlayers.size()>=MAX_REGISTRATED_PLAYERS){ 
			return false;
		}
		return true;
	}
    /**
     * Method is used for calculating National team rating
     */
	@Override
	public double calculateRating() {
		double skillImportance=0.3;// represents 30%
		double spiritImportance=0.7;//represents 70%
		return skillImportance*calculateTeamSkill()+spiritImportance*calculateTeamSpirit();
	}

}
