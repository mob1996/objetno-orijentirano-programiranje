package hr.fer.oop.lab4.prob2;

import java.util.HashSet;
import java.util.LinkedHashSet;


public class Demonstration {

	public static void main(String[] args) {
		  System.out.println("Stvaranje novog tima...");
          ClubTeam varteks = null;
          try {
                  varteks = new ClubTeam("VARTEKS", Formation.F352, 101);
          } catch (IllegalArgumentException e) {
                  System.out.println("Gre�ka: " + e.toString() + "\nOporavak od pogre�ke..");
                  varteks = new ClubTeam("VARTEKS", Formation.F352, 75);
          }
          System.out.println("Naziv:" + varteks.getName() + ", reputacija:" + varteks.getReputation() + ", formacija:"
                          + varteks.getTeamFormation());
          
          FootballPlayer[] players=new FootballPlayer[6];
  		HashSet<FootballPlayer> igraci=new LinkedHashSet<>();
  		try{
  		players[0]=new FootballPlayer("Ronaldo","Brazil",93,95,PlayingPosition.FW);
  		} catch (IllegalArgumentException ex){
  			System.out.println("Greska: " +ex+"\nOporavak od pogreske...");
  			players[0]=new FootballPlayer("Ronaldo","Brazil",93,95,PlayingPosition.FW);
  		}
  		
  		try{
  		players[1]=new FootballPlayer("Modric","Hrvatska",80,85,PlayingPosition.MF);
  		} catch(IllegalArgumentException ex){
  			System.out.println("Greska: " +ex+"\nOporavak od pogreske...");
  			players[1]=new FootballPlayer("Modric","Hrvatska",80,85,PlayingPosition.MF);
  		}
  		
  		try{
  		players[2]=new FootballPlayer("Modric","Hrvatska",80,87,PlayingPosition.MF);
  		} catch(IllegalArgumentException ex){
  			System.out.println("Greska: " +ex+"\nOporavak od pogreske...");
  			players[2]=new FootballPlayer("Modric","Hrvatska",80,87,PlayingPosition.MF);
  		}
  		
  		try{
  		players[3]=new FootballPlayer("Messi","Argentina",90,87,PlayingPosition.FW);
  		} catch(IllegalArgumentException ex){
  			System.out.println("Greska: " +ex+"\nOporavak od pogreske...");
  			players[3]=new FootballPlayer("Messi","Argentina",90,87,PlayingPosition.FW);
  		}
  		
  		try{
  		players[4]=new FootballPlayer("Van der Saar","Nizozemska",90,90,PlayingPosition.FW);
  		} catch(IllegalArgumentException ex){
  			System.out.println("Greska: " +ex+"\nOporavak od pogreske...");
  			players[4]=new FootballPlayer("Van der Saar","Nizozemska",90,90,PlayingPosition.GK);
  		}
  		
  		players[5]=new FootballPlayer("Pjaca","Hrvatska",73,70,PlayingPosition.MF);
  		
  			
  		for(FootballPlayer p:players){
  			try{
  			varteks.registerPlayer(p);
  			} catch(NotEligiblePlayerException ex){
  				System.out.println(ex);
  			}
  		}
  		varteks.unregisterPlayer(players[0]);
  		
  		for(FootballPlayer p:varteks.registratedPlayers){
  			System.out.println(p);
  		}
  		
  		System.out.println(varteks.getName()+" is ready for match: "+varteks.isMatchReady());
  		
  		
         
          
	}

}
