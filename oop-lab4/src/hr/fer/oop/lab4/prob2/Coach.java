package hr.fer.oop.lab4.prob2;
/**
 * 
 * @author Luka
 * Class that contains ever needed information for football coach
 *
 */
public class Coach extends Person {
	private int coachingSkill;
	private Formation favoriteFormation;
	/**
	 * Only constructor that can be used for creating instance of class Coach
	 * @param name Name of the Coach
	 * @param country  Coach's country of origin
	 * @param emotion  Coach's emotion on scale 0-100
	 * @param coachingSkill Coach's coaching skill on scale 0-100
	 * @param favoriteFormation Coach's favorite formation
	 */
	public Coach(String name, String country, int emotion, int coachingSkill, Formation favoriteFormation){
		super(name,country, emotion);
		if(coachingSkill>100 || coachingSkill<0){
			throw new IllegalArgumentException("coachingSkill must be above 0 and below 100");
		}
		this.coachingSkill=coachingSkill;
		if(favoriteFormation==null){
			throw new IllegalArgumentException("favoriteFormation can not be null");
		}
		this.favoriteFormation=favoriteFormation;
	}

	public int getCoachingSkill() {
		return coachingSkill;
	}

	public void setCoachingSkill(int coachingSkill) {
		this.coachingSkill = coachingSkill;
	}

	public Formation getFavoriteFormation() {
		return favoriteFormation;
	}

	public void setFavoriteFormation(Formation favoriteFormation) {
		this.favoriteFormation = favoriteFormation;
	}
	
	/**
	 * Method used for turning class coach in writable string
	 */
	@Override
	public String toString(){
		return "(Country: "+getCountry()+"- "+getName()+"- Skill: "+ coachingSkill+"- Favorite formation: "
				+ favoriteFormation+"- Emotion: "+getEmotion()+")";
	}
	

}
