package hr.fer.oop.lab4.prob2;
/**
 * 
 * @author Luka
 * Class that contains ever needed information for football player
 */
public class FootballPlayer extends Person {
   private int playingSkill;
   PlayingPosition playingPosition;
   /**
    * Only constructor that can be used for creating instance of class FootballPlayer
    * @param name Player's name
    * @param country Player's country of origin
    * @param emotion Player's emotion on scale 0-100
    * @param playingSkill Player's playing skill on scale 0-100
    * @param playingPosition Player's playing position
    */
   public FootballPlayer(String name, String country, int emotion,
		  int playingSkill, PlayingPosition playingPosition){
	   super(name,country,emotion);
	   if(playingSkill<0 || playingSkill>100){
		   throw new IllegalArgumentException("playingSkill must be above 0 and below 100");
	   }
	   this.playingSkill=playingSkill;
	   if(playingPosition==null){
			throw new IllegalArgumentException("playingPosition can not be null");
	   }
	   this.playingPosition=playingPosition;
   }

   public int getPlayingSkill() {
	   return playingSkill;
   }

   public void setPlayingSkill(int playingSkill) {
	   this.playingSkill = playingSkill;
   }

   public PlayingPosition getPlayingPosition() {
	   return playingPosition;
   }

   public void setPlayingPosition(PlayingPosition playingPosition) {
	   this.playingPosition = playingPosition;
   }
   @Override
	public String toString(){
		return "(Country: "+getCountry()+"- "+getName()+"- Skill: "+ playingSkill+"- Playing position: "
				+ playingPosition+"- Emotion: "+getEmotion()+")";
	}
   
 
   
}
