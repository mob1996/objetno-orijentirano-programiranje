package hr.fer.oop.lab4.prob2;
/**
 * 
 * @author Luka
 * Class is used for defining football club, it
 * extends class Team
 *
 */
public class ClubTeam extends Team {
   public static final int MAX_REGISTRATED_PLAYERS=25;
   private int reputation;
   
   /**
    * Public constructor used for creating instance of ClubTeam
    * @param name Name of the club
    * @param teamFormation Club's playing formation
    * @param reputation Club's reputation
    */
   public ClubTeam(String name, Formation teamFormation, int reputation){
	   super(name,teamFormation);
	   if(reputation<0 || reputation>100){
		   throw new IllegalArgumentException("Reputation must be element of [0,100]");
	   }
	   this.reputation=reputation;
   }

   public int getReputation() {
	   return reputation;
   }

   public void setReputation(int reputation) {
	   this.reputation = reputation;
   }
   
   /**
    * Method is used for determinate if player can play for national team
    */
   @Override
   public boolean isPlayerRegistrable(FootballPlayer player){
	   if(player.getPlayingSkill()<reputation || registratedPlayers.size()>=MAX_REGISTRATED_PLAYERS){
		   return false;
	   }
	   return true;
   }
   /**
    * Method is used for adding given player to registered player list.
    */
   @Override
   public void registerPlayer(FootballPlayer player){
	   if(!isPlayerRegistrable(player)){
		  throw new NotEligiblePlayerException("Player "+player.getName()+" can not be registrated because list is full"
				                              + " or because he is not good enought"); 
	   }
	   if(!registratedPlayers.add(player)){ 
			  throw new NotEligiblePlayerException("Player "+player.getName()+" is already in the team");
	   }
	   
				  
   }
   /**
    * Method is used for calculating national team rating
    */
   @Override
   public double calculateRating(){
	   double skillImportance=0.7; //represents 70%
	   double spiritImportance=0.3;//represents 30%
	   return skillImportance*calculateTeamSkill()+spiritImportance*calculateTeamSpirit();
   }
   
   @Override 
   public String toString(){
	   String output="Registrated players:\n";
	   for(FootballPlayer player:registratedPlayers){
		   output=output+player+" ";
	   }
	   output+="\nStarting eleven:\n";
	   for(FootballPlayer player:startingEleven){
		   output=output+player+" ";
	   }
	   return output;
   }

   
   
	
	

}
