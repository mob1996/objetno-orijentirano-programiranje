package hr.fer.oop.lab5.shell;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;

public class FilterFileVisitor implements FileVisitor<Path> {
	String firstPart;
	String secondPart;
	Environment env;
	
	public FilterFileVisitor(String firstPart,String secondPart, Environment env){
		this.firstPart=firstPart.toLowerCase();
		this.secondPart=secondPart.toLowerCase();
		this.env=env;
	}
    
	@Override
	public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
		return FileVisitResult.CONTINUE;
	}
/**
 * Method is used when visitor reaches a file. If it satisfies an if contition 
 * it prints its path.
 */
	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		String help=file.getFileName().toString().toLowerCase();
		if(help.startsWith(firstPart) && help.endsWith(secondPart)){
			env.writeln(file.toString());
		}
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
		return FileVisitResult.CONTINUE;
	}

}
