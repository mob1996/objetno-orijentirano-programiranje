package hr.fer.oop.lab5.shell;

import java.nio.file.Path;

public interface Environment {
   public String readLine();
   public void write(String str);
   public void writeln(String str);
   public Iterable<ShellCommand> commands();
   public Path getCurrentPath();
   public void setCurrentPath(Path newPath);
}
