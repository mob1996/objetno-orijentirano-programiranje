package hr.fer.oop.lab5.shell;

public interface ShellCommand {
   public String getCommandName();
   public String getCommandDescription();
   public CommandStatus execute(Environment env, String str);
}
