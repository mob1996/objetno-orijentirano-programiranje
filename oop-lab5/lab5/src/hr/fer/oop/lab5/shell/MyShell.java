package hr.fer.oop.lab5.shell;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileAttribute;
import java.util.HashMap;
import java.util.Map;
/**
 * 
 * @author Luka Mijic
 *
 */
public class MyShell {

	private static Map<String, ShellCommand> commands;
	
	static{
		commands=new HashMap<>();
		ShellCommand[] cc={
				new HelpCommand(),
				new DateCommand(),
				new PwdCommand(),
				new QuitCommand(),
				new CdCommand(),
				new TypeCommand(),
				new CopyCommand(),
				new FilterCommand(),
				new XCopyCommand()
		};
		for(ShellCommand c:cc){
			commands.put(c.getCommandName(), c);
		}
	}
	/**
	 * 
	 * Class is used for simulating enviroment, that allows us to
	 * communicate via keyboard with PC.
	 *
	 */
	public static class EnvironmentImpl implements Environment{
        BufferedReader reader= new BufferedReader(
        		               new InputStreamReader(System.in));
        BufferedWriter writer= new BufferedWriter(
        		               new OutputStreamWriter(System.out));
        Path currentPath;
       
        public EnvironmentImpl(){
        	currentPath=Paths.get("").toAbsolutePath();
        }
		/**
		 * Reads line from keyboard.
		 */
		@Override
		public String readLine() {
			String readText=null;
			try {
				readText=reader.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return readText;
		}
        /**
         * Writes on screen
         */
		@Override
		public void write(String str) {
			try {
				writer.write(str);
				writer.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
        
		/**
		 * Method is used to write one line
		 */
		@Override
		public void writeln(String str) {
			try {
				writer.write(str+"\n");
				writer.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}

		@Override
		public Iterable<ShellCommand> commands() {
			return commands.values();
		}

		@Override
		public Path getCurrentPath() {
			return currentPath;
		}

		@Override
		public void setCurrentPath(Path newPath) {
			currentPath=newPath;
			
		}
		
	}
	
	
	
	
	
	
	public static Environment environment=new EnvironmentImpl();
	/**
	 * This method represents a shell.
	 * @param args
	 */
	public static void main(String[] args) {
		environment.writeln("Welcome to MyShell! You may enter commands.");
		while(true){
			int nameCount=environment.getCurrentPath().getNameCount();
			if(nameCount<1){
				environment.write(environment.getCurrentPath().toString()+" ");
			} else {
			    environment.write("$"+environment.getCurrentPath()
                                 .getName(environment.getCurrentPath().getNameCount()-1)
                                  +"> ");
			}
			String line=environment.readLine();
			String cmd=line.split(" ",2)[0].toUpperCase();
			String arg="";
			if(line.split(" ", 2).length>1){
				arg=line.split(" ", 2)[1].trim();
			}
			ShellCommand shellCommand=commands.get(cmd);
			if(shellCommand==null){
				environment.writeln("Unknown command!");
				continue;
			}
			if(shellCommand.execute(environment, arg).equals(CommandStatus.EXIT)){
				break;
			}
		}
		environment.writeln("Thank you for using this shell. Goodbye!");
		
		
	}

}
