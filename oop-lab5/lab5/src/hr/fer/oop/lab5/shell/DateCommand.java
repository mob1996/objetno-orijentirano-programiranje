package hr.fer.oop.lab5.shell;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateCommand extends AbstractCommand {

	public DateCommand() {
		super("DATE", "Shows current time in format year-month-day_hour-minute-second");
	}
/**
 * Method is used to show current time.
 */
	@Override
	public CommandStatus execute(Environment env, String str) {
		String currentTime=new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(
				               Calendar.getInstance().getTime());
		env.writeln(currentTime);
		return CommandStatus.CONTINUE;
	}
	

}
