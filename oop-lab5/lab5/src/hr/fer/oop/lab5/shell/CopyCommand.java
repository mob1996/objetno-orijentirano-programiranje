package hr.fer.oop.lab5.shell;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CopyCommand extends AbstractCommand {

	public CopyCommand() {
		super("COPY", "Copies from source to destination");
		
	}
/**
 * Method is used to copy a file to another location.
 */
	@Override
	public CommandStatus execute(Environment env, String str) {
		String source;
		String destination;
		if(str.split(" ").length!= 2){
			env.writeln("Invalid number of arguments");
			return CommandStatus.CONTINUE;
		} else {
			source=str.split(" ")[0].trim();
			destination=str.split(" ")[1].trim();
		}
		Path sourcePath=Paths.get(source).toAbsolutePath();
		Path destinationPath=Paths.get(destination).toAbsolutePath();
		File sourceFile=sourcePath.toFile();
		File destinationFile;
		if(!sourceFile.exists()){
			env.writeln("Source file doesn't exist");
			return CommandStatus.CONTINUE;
		}
		int indexOfLastDot=source.lastIndexOf(".");
		String ext=source.substring(indexOfLastDot);
		if(destination.endsWith(ext)){
			destinationFile=destinationPath.toFile();
			if(!destinationFile.getParentFile().exists()){
				env.writeln("Path to file doesnt exist");
				return CommandStatus.CONTINUE;
			} else {
				try {
					destinationFile.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else{
			destinationFile=destinationPath.toFile();
			if(!destinationFile.exists()){
				env.writeln("Directory doesnt exist");
				return CommandStatus.CONTINUE;
			} else {
				destinationPath=Paths.get(destinationPath.toString(), sourcePath.getFileName().toString());
				destinationFile=destinationPath.toFile();
				try {
					destinationFile.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
		}
		copy(sourceFile,destinationFile);
		
		return CommandStatus.CONTINUE;
	}
	/**
	 * Method that is used as a help to execute method.
	 * @param source
	 * @param dest
	 */
	public static void copy (File source, File dest){
		try(InputStream in=new FileInputStream(source);
			OutputStream out=new FileOutputStream(dest);) {
			byte[] buf=new byte[1024];
			while(in.read(buf)>0){
				out.write(buf);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	
	

}
