package hr.fer.oop.lab5.shell;

import java.util.Iterator;



public class HelpCommand extends AbstractCommand {

	public HelpCommand() {
		super("HELP", "Prints out all commands");
		
	}
    /**
     * Method prints all other commands and their descriptions
     */
	@Override
	public CommandStatus execute(Environment env, String str) {
		env.writeln("Command Table");
		for(ShellCommand c:env.commands()){
			env.writeln(c.toString());
		}
		return CommandStatus.CONTINUE;
	}
	
	

}
