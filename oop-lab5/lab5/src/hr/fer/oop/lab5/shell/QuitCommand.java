package hr.fer.oop.lab5.shell;

public class QuitCommand extends AbstractCommand {

	public QuitCommand() {
		super("QUIT","Terminates shell");
		
	}
    /**
     * Method returns CommandStatus.EXIT, which terminates shell.
     */
	@Override
	public CommandStatus execute(Environment env, String str) {
		return CommandStatus.EXIT;
	}

}
