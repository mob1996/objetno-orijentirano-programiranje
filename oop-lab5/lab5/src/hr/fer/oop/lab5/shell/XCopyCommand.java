package hr.fer.oop.lab5.shell;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

public class XCopyCommand extends AbstractCommand {

	public XCopyCommand() {
		super("XCOPY", "xcopy");
		// TODO Auto-generated constructor stub
	}
/**
 * 
 */
	@Override
	public CommandStatus execute(Environment env, String str) {
		String source;
		String dest;
		if(str.split(" ").length!=2){
			env.writeln("Need 2 arguments");
			return CommandStatus.CONTINUE;
		} else {
			source=str.split(" ")[0];
			dest=str.split(" ")[1];
		}
		File sourceFile=new File(source);
		File destFile=new File(dest);
		if(sourceFile.isDirectory() && destFile.isDirectory()){
			destFile=new File(destFile,sourceFile.getName());
			recursiveCopy(sourceFile, destFile);
			return CommandStatus.CONTINUE;
		} else if(sourceFile.isDirectory() && destFile.getParentFile().isDirectory()){
			destFile.mkdir();
			recursiveCopy(sourceFile, destFile);
			return CommandStatus.CONTINUE;
		}
		
		return CommandStatus.CONTINUE;
	}
	
	private void recursiveCopy(File src, File dest){
		if(src.isDirectory()){
		   if(!dest.exists()){
			   dest.mkdir();
		   }
		   String files[]=src.list();
		   for(String file:files){
			   File srcFile=new File(src,file);
			   File destFile=new File(dest,file);
			   recursiveCopy(srcFile,destFile);
		   }
		} else {
			CopyCommand.copy(src, dest);
		}
		
	}
	
	
	

}
