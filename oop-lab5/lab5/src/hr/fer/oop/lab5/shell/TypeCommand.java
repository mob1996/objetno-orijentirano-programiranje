package hr.fer.oop.lab5.shell;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

public class TypeCommand extends AbstractCommand {

	
	public TypeCommand() {
		super("TYPE", "Prints contents of a file");
		// TODO Auto-generated constructor stub
	}
/**	
 * Method prints contents from file to screen.
 */ 
	@Override
	public CommandStatus execute(Environment env, String str) {
		Path targetPath;
		if(Paths.get(str).isAbsolute()){
		    targetPath=Paths.get(str);
		} else {
			targetPath=Paths.get(env.getCurrentPath().toString(),str);
		}
		if(Files.isRegularFile(targetPath, LinkOption.NOFOLLOW_LINKS)){
			if(Files.isReadable(targetPath)){
				try (BufferedReader fileReader=new BufferedReader(
							                  new InputStreamReader(
							                  new BufferedInputStream(
							                  new FileInputStream(targetPath.toString())),"UTF-8"));)
				{
					while(true){
						String line=fileReader.readLine();
						if(line==null){
							break;
						}
						env.writeln(line);
					}
					
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			} else {
				env.writeln(targetPath.toString()+" is not readable.");
			}
		} else {
			env.writeln(targetPath.toString()+" is not a path to file");
		}
		return CommandStatus.CONTINUE;
	}

}
