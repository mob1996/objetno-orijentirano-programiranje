package hr.fer.oop.lab5.shell;

import java.io.IOException;
import java.nio.file.Files;

public class FilterCommand extends AbstractCommand {

	public FilterCommand() {
		super("FILTER", "Filters files");
		// TODO Auto-generated constructor stub
	}
/**
 * Method is used to filter files.
 */
	@Override
	public CommandStatus execute(Environment env, String str) {
		FilterFileVisitor visitor;
		if(!str.contains("*")){
			visitor=new FilterFileVisitor(str,str,env);
		} else if(str.split("\\*",2).length==2){
			visitor=new FilterFileVisitor(str.split("\\*",2)[0].trim(),
					str.split("\\*",2)[1].trim(), env);
		} else {
			env.writeln("There can be only one character *");
			return CommandStatus.CONTINUE;
		}
		try {
			Files.walkFileTree(env.getCurrentPath(), visitor);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return CommandStatus.CONTINUE;
	}

}
