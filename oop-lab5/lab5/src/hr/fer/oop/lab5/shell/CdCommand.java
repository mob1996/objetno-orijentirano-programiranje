package hr.fer.oop.lab5.shell;

import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CdCommand extends AbstractCommand{

	public CdCommand() {
		super("CD","Changes current directory");
	}
    
	/**
	 * Method is used to position yourself in directory.
	 */
	@Override
	public CommandStatus execute(Environment env, String str) {
		if(!Paths.get(str).isAbsolute()){
			if(str.equals(".")){
				return CommandStatus.CONTINUE;
			} else if(str.equals("..")){
				if(env.getCurrentPath().getNameCount()<1){
					return CommandStatus.CONTINUE;
				}
				Path newPath=env.getCurrentPath().getParent();
				env.setCurrentPath(newPath);
				env.writeln("Current directory is now set to "+env.getCurrentPath().toString());
				return CommandStatus.CONTINUE;
			} else {
				Path newPath=Paths.get(env.getCurrentPath().toString(),str);
				if(Files.isDirectory(newPath, LinkOption.NOFOLLOW_LINKS)){
					env.setCurrentPath(newPath);
					env.writeln("Current directory is now set to "+env.getCurrentPath().toString());
					return CommandStatus.CONTINUE;
				} else {
					env.writeln("The system cannot find the path specified");
					return CommandStatus.CONTINUE;
				}
				
			}
		} else {
			Path newPath=Paths.get(str);
			if(Files.isDirectory(newPath)){
				env.setCurrentPath(newPath);
				env.writeln("Current directory is now set to "+env.getCurrentPath().toString());
				return CommandStatus.CONTINUE;
			} else {
				env.writeln("The system cannot find the path specified");
				return CommandStatus.CONTINUE;		
			}
		}
		
	}
	

}
