package hr.fer.oop.lab5.shell;

public abstract class AbstractCommand implements ShellCommand {
	
    String commandName;
    String commandDescription;
    
    public AbstractCommand(String commandName, String commandDescription){
        this.commandName=commandName;
        this.commandDescription=commandDescription;
    }
    
	@Override
	public String getCommandName() {
		return commandName;
	}

	@Override
	public String getCommandDescription() {
		return commandDescription;
	}
	/**
	 * Turns command into String
	 */
	@Override
	public String toString(){
		return String.format("%s - %s", commandName, commandDescription);
	}

}
