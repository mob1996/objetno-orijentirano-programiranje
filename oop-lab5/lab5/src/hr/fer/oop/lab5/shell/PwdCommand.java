package hr.fer.oop.lab5.shell;

public class PwdCommand extends AbstractCommand {

	public PwdCommand() {
		super("PWD", "Shows current path");
		
	}
    /**
     * Method prints absolute path.
     */
	@Override
	public CommandStatus execute(Environment env, String str) {
		env.writeln(env.getCurrentPath().toString());
		return CommandStatus.CONTINUE;
	}

}
