package hr.fer.oop.lab5.exams;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hr.fer.oop.lab5.shell.CommandStatus;
import hr.fer.oop.lab5.shell.Environment;
import hr.fer.oop.lab5.shell.ShellCommand;
import hr.fer.oop.lab5.shell.MyShell.EnvironmentImpl;


/**
 * 
 * @author Luka Mijic
 *
 */
public class ExamShell {
    private static Map<String, ExamShellCommand> examCommands;
	
	static{
		examCommands=new HashMap<>();
		ExamShellCommand[] cc={
				new LoadCommand(),
				new FilterCommand(),
				new ResetCommand(),
				new StatisticsCommand(),
				new ProblemStatistics(),
				new StudentResult(),
				new PrintCommand(),
				new QuitCommand(),
		};
		for(ExamShellCommand c:cc){
			examCommands.put(c.getCommandName(), c);
		}
	}
	/**
	 *Class is used for simulating enviroment, that allows us to
	 *communicate via keyboard with PC.
	 */
	public static class ExamEnvironmentImpl implements ExamEnvironment{
		BufferedReader reader= new BufferedReader(
	               new InputStreamReader(System.in));
        BufferedWriter writer= new BufferedWriter(
	               new OutputStreamWriter(System.out));
        
        /**
		 * Reads line from keyboard.
		 */
		@Override
		public String readLine() {
			String readText=null;
			try {
				readText=reader.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return readText;
		}

		/**
         * Writes on screen
         */
		@Override
		public void write(String str) {
			try {
				writer.write(str);
				writer.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		/**
		 * Method is used to write one line
		 */
		@Override
		public void writeln(String str) {
			try {
				writer.write(str+"\n");
				writer.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}

		
		
	}
	
	public static ExamEnvironment environment=new ExamEnvironmentImpl();
	/**
	 * This method represents a shell.
	 * @param args
	 */
	public static void main(String[] args) {
		environment.writeln("Welcome to ExamShell! You may enter commands.");
		while(true){
			environment.write("Enter command: ");
			String line=environment.readLine();
			String cmd=line.split(" ",2)[0].toUpperCase();
			String arg="";
			if(line.split(" ", 2).length>1){
				arg=line.split(" ", 2)[1].trim();
			}
			ExamShellCommand examShellCommand=examCommands.get(cmd);
			if(examShellCommand==null){
				environment.writeln("Unknown command!");
				continue;
			}
			if(examShellCommand.execute(environment, arg).equals(CommandStatus.EXIT)){
				break;
			}
		}
		
	}
	
	
	
}
