package hr.fer.oop.lab5.exams;

import java.util.List;

import hr.fer.oop.lab5.shell.CommandStatus;

public class ProblemStatistics extends ExamCommand {
    private static List<SheetData> probStatList;
	
	public ProblemStatistics() {
		super("PROBLEM-STATISTICS");
	}
/**
 * Prints how many people answered with given answers from every question, 
 * using only sheet from active list.
 */
	@Override
	public CommandStatus execute(ExamEnvironment env, String str) {
		probStatList=LoadCommand.getActiveSheets();
		if(!str.isEmpty()){
			env.writeln("Problem-statistics doesn't have arguments");
		} else if(probStatList.isEmpty()){
			env.writeln("Cannot calculate statistic on empty list");
		}
		for(int questionNum=0;questionNum<LoadCommand.getCorrectAnswers().get("A").length;questionNum++){
			env.writeln("Question: "+(questionNum+1)+".");
			calcAndPrintForQuestion(env, questionNum);
		}
		
		
		return CommandStatus.CONTINUE;
	}
	
	private void calcAndPrintForQuestion(ExamEnvironment env,int questionNum){
		String[] allPossibleAnswers={"A","B","C","D","E","BLANK"};
		long ansCount;
		for(String ans:allPossibleAnswers){
			ansCount=probStatList.stream()
					.filter(sheet->sheet.getAnswers().get(questionNum).equals(ans))
					.count();
			env.writeln("   Answer: "+ans+"-"+ansCount);
		}
		
	}

}
