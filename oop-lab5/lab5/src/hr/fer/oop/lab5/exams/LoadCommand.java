package hr.fer.oop.lab5.exams;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;

import hr.fer.oop.lab5.shell.CommandStatus;


public class LoadCommand extends ExamCommand {
	private static List<SheetData> activeSheets=new LinkedList<>();
    private static List<SheetData> allSheets;
    private static Map<String, String[]> correctAnswers;
    private static Map<String,SheetData> resultsMap=new HashMap<>();
    private static double valueOfCorrectAns;
    private static double valueOfIncorrectAns;
    private static double valueOfBlankAns;
    
	public LoadCommand() {
		super("LOAD");
	}

    
	public static void setActiveSheets(List<SheetData> activeSheets) {
		LoadCommand.activeSheets = activeSheets;
	}


	public static List<SheetData> getActiveSheets() {
		return activeSheets;
	}

	public static List<SheetData> getAllSheets() {
		return allSheets;
	}


	public static Map<String, String[]> getCorrectAnswers() {
		return correctAnswers;
	}


	public static Map<String, SheetData> getResultsMap() {
		return resultsMap;
	}

/**
 * Method loads sheets and correct answers from paths, and then loads list
 * and maps with them. 
 */
	@Override
	public CommandStatus execute(ExamEnvironment env, String str) {
		String[] args=str.split(" ");
		if(args.length!=5){
			env.writeln("Invalid number of arguments!");
			return CommandStatus.CONTINUE;
		}
		Path examSheetsPath=Paths.get(args[0]).toAbsolutePath();
		Path answersPath=Paths.get(args[1]).toAbsolutePath();
		try{
		     valueOfCorrectAns=Double.valueOf(args[2]);
		     valueOfIncorrectAns=Double.valueOf(args[3]);
		     valueOfBlankAns=Double.valueOf(args[4]);
		} catch (NumberFormatException c) {
			env.writeln("Last 3 arguments must be numbers");
			return CommandStatus.CONTINUE;
		}
		
		
		allSheets=SheetDataLoader.loadSheets(examSheetsPath);
		correctAnswers=SheetDataLoader.loadCorrectAnswers(answersPath);
		
		
		for(SheetData sheet:allSheets){
			
		    if(sheet.getGroup().equals("BLANK")){
				env.writeln("Student "+sheet.getJmbag()+" didn't"
						+ " write down his group");
				activeSheets.add(sheet);
				continue;
			} else if(correctAnswers.get(sheet.getGroup())==null){
				env.writeln("Student "+ sheet.getJmbag()+" used"
						 +" invalid group");
				activeSheets.add(sheet);
				continue;
			}
		    
		    List<AnswerScore> scoreList=valuatingStudentsAnswers(sheet.getAnswers(),
		    		correctAnswers.get(sheet.getGroup()), valueOfCorrectAns, valueOfIncorrectAns,
		    		valueOfBlankAns);
		    sheet.setAnswerScores(scoreList);
		    double studentScore=totalScore(scoreList);
		    sheet.setTotalScore(OptionalDouble.of(studentScore));
		    activeSheets.add(sheet);
		    resultsMap.put(sheet.getJmbag(), sheet);
		    
		}
		
		OptionalDouble average=allSheets.stream().filter((sheet)->sheet.getTotalScore()!=OptionalDouble.empty())
				.mapToDouble((sheet)->sheet.getTotalScore().getAsDouble()).average();
		if(average!=OptionalDouble.empty()){
			env.writeln("Average : "+average.getAsDouble());
		} else {
			env.writeln("There is no average!\nAll students are invalid or there are none.");
		}
		
		
		
		return CommandStatus.CONTINUE;
	}
	
	/**
	 * Method is used for valuating students answers.
	 * @param studentAnswers
	 * @param answers
	 * @param valueOfCorrectAns
	 * @param valueOfIncorrectAns
	 * @param valueOfBlankAns
	 * @return
	 */
	private List<AnswerScore> valuatingStudentsAnswers(List<String> studentAnswers, String[] answers,
			double valueOfCorrectAns, double valueOfIncorrectAns, double valueOfBlankAns){
		List<AnswerScore> result=new LinkedList<>();
		int questionOrder=0;
		for(String answer:studentAnswers){
			if(answer.equals(answers[questionOrder])){
				result.add(new AnswerScore(valueOfCorrectAns,AnswerStatus.ANSWERED_CORRECT));
			} else if(answer.equals("BLANK")){
				result.add(new AnswerScore(valueOfBlankAns, AnswerStatus.UNANSWERED));
			} else {
				result.add(new AnswerScore(valueOfIncorrectAns,AnswerStatus.ANSWERED_INCORRECT));
			}
			questionOrder++;
		}
		
		return result;
	}
	
	private double totalScore(List<AnswerScore> answerScoreList){
		double score=0;
		for(AnswerScore scoreOfCurrentQuestion:answerScoreList){
			score+=scoreOfCurrentQuestion.getScore();
		}
		return score;
	}
	
	

}
