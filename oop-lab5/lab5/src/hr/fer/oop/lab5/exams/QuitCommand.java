package hr.fer.oop.lab5.exams;

import hr.fer.oop.lab5.shell.CommandStatus;

public class QuitCommand extends ExamCommand {

	public QuitCommand() {
		super("QUIT");
	}
/**
 * 	Method returns CommandStatus.EXIT, which terminates shell.
 */
	@Override
	public CommandStatus execute(ExamEnvironment env, String str) {
		return CommandStatus.EXIT;
	}

}
