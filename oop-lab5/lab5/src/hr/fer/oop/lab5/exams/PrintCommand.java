package hr.fer.oop.lab5.exams;





import java.util.Collections;
import java.util.List;

import hr.fer.oop.lab5.shell.CommandStatus;

public class PrintCommand extends ExamCommand {
    private static List<SheetData> printList;
    
	public PrintCommand() {
		super("PRINT");
	}
/**
 * Method is used to print all sheets from active list
 */
	@Override
	public CommandStatus execute(ExamEnvironment env, String str) {
		printList=LoadCommand.getActiveSheets();
		if(!str.isEmpty()){
			env.writeln("Print command doesnt need arguments");
			return CommandStatus.CONTINUE;
		} else if(printList.isEmpty()){
			env.writeln("List cannot be empty");
			return CommandStatus.CONTINUE;
		}
		
		Collections.sort(printList);
		env.writeln("Number of active sheets: "+printList.size());
		for(SheetData sheet:printList){
			env.writeln(sheet.toString());
		}
		
		
		return CommandStatus.CONTINUE;
	}
	
	
	

}
