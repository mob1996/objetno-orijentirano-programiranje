package hr.fer.oop.lab5.exams;

import hr.fer.oop.lab5.shell.CommandStatus;

public class ResetCommand extends ExamCommand {

	public ResetCommand() {
		super("RESET");
	}
/**
 * Method resets active list to all loaded sheets
 */
	@Override
	public CommandStatus execute(ExamEnvironment env, String str) {
		if(!str.isEmpty()){
			env.writeln("Command reset doesn't have arguments");
		}
		LoadCommand.setActiveSheets(LoadCommand.getAllSheets());
		return CommandStatus.CONTINUE;
	}

}
