package hr.fer.oop.lab5.exams;

import hr.fer.oop.lab5.shell.CommandStatus;

public interface ExamShellCommand {
	public String getCommandName();
    public CommandStatus execute(ExamEnvironment env, String str);
}
