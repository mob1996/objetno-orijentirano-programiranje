package hr.fer.oop.lab5.exams;

import java.util.List;

public interface ExamEnvironment {
   public String readLine();
   public void write(String str);
   public void writeln(String str);
   
}
