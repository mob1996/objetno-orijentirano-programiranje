package hr.fer.oop.lab5.exams;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class SheetDataLoader {
	/**
	 * Method is used to fill list sheets with students and their answers
	 * @param sheetPath
	 * @return
	 */
	public static List<SheetData> loadSheets(Path sheetPath){
		List<SheetData> sheets=new LinkedList<>();
		sheetPath=sheetPath.toAbsolutePath();
		String jmbag;
		String group;
		try(BufferedReader br=new BufferedReader(
				              new InputStreamReader(
				              new BufferedInputStream(
				              new FileInputStream(sheetPath.toString())),"UTF-8"));
		){
			String line;
			while(true){
				line=br.readLine();
				if(line==null){
					break;
				}
				List<String> answers=new ArrayList<>();
				String[] lineValues=line.split("\t");
				jmbag=lineValues[0];
				group=lineValues[1];
				for(int i=2;i<lineValues.length;i++){
					answers.add(lineValues[i]);
				}
				sheets.add(new SheetData(jmbag,group,answers));
			}
			
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			System.out.println("File not found!");
		} catch (IOException e) {
			e.printStackTrace();
		} 
		
		
		
		return sheets;
	}
	
	
	/**
	 * Method is used to fill list correctAnswers with groups and their correct answers.
	 * @param correctPath
	 * @return
	 */
	public static Map<String,String[]> loadCorrectAnswers(Path correctPath){
		Map<String,String[]> correctAnswers=new HashMap<>();
		correctPath=correctPath.toAbsolutePath();
		try(BufferedReader br=new BufferedReader(
	              new InputStreamReader(
	              new BufferedInputStream(
	              new FileInputStream(correctPath.toString())),"UTF-8"));
        ){  
			String line;
			while((line=br.readLine())!=null){
				String[] strings=line.split("\t");
				String group=strings[0];
				String[] answers=new String[20];
				for(int i=1;i<strings.length;i++){
					answers[i-1]=strings[i];
				}
				correctAnswers.put(group, answers);
			}
			
			
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			System.out.println("File not found!");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return correctAnswers;
	}

}
