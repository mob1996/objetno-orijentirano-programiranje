package hr.fer.oop.lab5.exams;


import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import hr.fer.oop.lab5.shell.CommandStatus;

public class FilterCommand extends ExamCommand {
	private static List<SheetData> filterList;
    
    
	public FilterCommand() {
		super("FILTER");
	}
	

	public static List<SheetData> getFilterList() {
		return filterList;
	}

/**
 * Method is used for filtering active list, all method below are 
 * methods that work with execute() in order for filtering to work.
 *  */
	@Override
	public CommandStatus execute(ExamEnvironment env, String str) {
		filterList=LoadCommand.getActiveSheets();
		if(str.isEmpty()){
			env.writeln("Filter command must have arguments!");
			return CommandStatus.CONTINUE;
		} else if(filterList.isEmpty()){
			env.writeln("Load command must be used first.");
			return CommandStatus.CONTINUE;
		}
		
		if(str.toLowerCase().equals("graded")){
			filterGraded(env);
		} else if(str.toLowerCase().equals("!graded")){
			filterNonGraded(env);
		} if(str.toLowerCase().startsWith("group=")){
			String group=str.split("=",2)[1].toUpperCase().trim();
			if(LoadCommand.getCorrectAnswers().get(group)==null){
				env.writeln("That group doesnt exist.");
			} else {
				filterGroup(env,group);
			}
		} else if(str.toLowerCase().startsWith("score>=")){
			String conditionString=str.split("=",2)[1].trim();
			try{
				double condition=Double.valueOf(conditionString);
				filterScoreEqualOrBigger(env,condition);
			} catch (NumberFormatException e){
				env.writeln("Argument for filter score command must be a number");
			}
			
		} else if(str.toLowerCase().startsWith("score<=")){
			String conditionString=str.split("=",2)[1].trim();
			try{
				double condition=Double.valueOf(conditionString);
				filterScoreEqualOrSmaller(env,condition);
			} catch (NumberFormatException e){
				env.writeln("Argument for filter score command must be a number");
			}
		} else if(str.split(" ").length==3){
			String[] args=str.split(" ");
			if(str.toLowerCase().startsWith("status")){
				try{
					int questionNumber=Integer.parseInt(args[1]);
					int numOfQuestions=LoadCommand.getCorrectAnswers().get("A").length;
					if(questionNumber<1 || questionNumber>numOfQuestions){
						throw new IllegalArgumentException("Question number out of bounds");
					}
					String ans=args[2];
					if(!ans.equals("CORRECT") && !ans.equals("INCORRECT") &&
					   !ans.equals("UNANSWERED")){
						throw new IllegalArgumentException("Question status is invalid");
					}
					filterStatus(env, questionNumber, ans);
				} catch (IllegalArgumentException exc){
					env.writeln(exc.toString());
				}
			} else if(str.toLowerCase().startsWith("answer")){
				try{
					int questionNumber=Integer.parseInt(args[1]);
					int numOfQuestions=LoadCommand.getCorrectAnswers().get("A").length;
					if(questionNumber<1 || questionNumber>numOfQuestions){
						throw new IllegalArgumentException("Question number out of bounds");
					}
					String ans=args[2];
					if(LoadCommand.getCorrectAnswers().values().contains(ans)){
						throw new IllegalArgumentException("There is no answer "+ans);
					}
					filterAnswer(env, questionNumber, ans);
			    } catch (IllegalArgumentException exc){
					env.writeln(exc.toString());
				}
			}
			
		} else {
			env.writeln("Invalid arguments");
		}
		
			
		return CommandStatus.CONTINUE;
	}
	
	
	/**
	 * Leaves only graded exams in the active list
	 * @param env
	 */
	private void filterGraded(ExamEnvironment env){
		env.writeln("List size before filter: "+filterList.size());
		filterList=filterList.stream().filter(score->score.getTotalScore()!=OptionalDouble.empty())
				.collect(Collectors.toList());
		LoadCommand.setActiveSheets(filterList);
		env.writeln("List size after filter: "+filterList.size());
		
	}
	/**
	 * Leaves only not graded exam in active list
	 * @param env
	 */
	private void filterNonGraded(ExamEnvironment env){
		env.writeln("List size before filter: "+filterList.size());
		filterList=filterList.stream().filter(score->score.getTotalScore()==OptionalDouble.empty())
				.collect(Collectors.toList());
		LoadCommand.setActiveSheets(filterList);
		env.writeln("List size after filter: "+filterList.size());
	}
	/**
	 * Leaves only group that is equal to parametar group in the list
	 * @param env
	 * @param group
	 */
	private void filterGroup(ExamEnvironment env, String group){
		env.writeln("List size before filter: "+filterList.size());
		filterList=filterList.stream().filter(sheet->sheet.getGroup().equals(group))
				.collect(Collectors.toList());
		LoadCommand.setActiveSheets(filterList);
		env.writeln("List size after filter: "+filterList.size());
	}
	
	/**
	 * Leaves only scores that are equal or higher than condition
	 * @param env
	 * @param condition
	 */
	private void filterScoreEqualOrBigger(ExamEnvironment env, double condition){
		env.writeln("List size before filter: "+filterList.size());
		filterList=filterList.stream()
				 .filter(sheet->!sheet.getTotalScore().equals(OptionalDouble.empty()))
				 .filter(sheet->sheet.getTotalScore().getAsDouble()>=condition)
				 .collect(Collectors.toList());
		LoadCommand.setActiveSheets(filterList);
		env.writeln("List size after filter: "+filterList.size());
	}
	/**
	 * Leaves only scores that are equal or smaller than condition
	 * @param env
	 * @param condition
	 */
	private void filterScoreEqualOrSmaller(ExamEnvironment env, double condition){
		env.writeln("List size before filter: "+filterList.size());
		filterList=filterList.stream()
				 .filter(sheet->!sheet.getTotalScore().equals(OptionalDouble.empty()))
				 .filter(sheet->sheet.getTotalScore().getAsDouble()<=condition)
				 .collect(Collectors.toList());
		LoadCommand.setActiveSheets(filterList);
		env.writeln("List size after filter: "+filterList.size());
	}
	/**
	 * Leaves only those whose question number questionNumber corresponds with answer ans.
	 * @param env
	 * @param questionNumber
	 * @param ans
	 */
	private void filterStatus(ExamEnvironment env, int questionNumber, String ans){
		env.writeln("List size before filter: "+filterList.size());
		AnswerStatus status;
		if(ans.equals("CORRECT")){
			status=AnswerStatus.ANSWERED_CORRECT;
		} else if(ans.equals("INCORRECT")){
			status=AnswerStatus.ANSWERED_INCORRECT;
		} else {
			status=AnswerStatus.UNANSWERED;
		}
		filterList=filterList.stream().filter(sheet->sheet.getTotalScore()!=OptionalDouble.empty())
				.filter(sheet->sheet.getAnswerScores().get(questionNumber-1).getStatus().equals(status))
				.collect(Collectors.toList());
		LoadCommand.setActiveSheets(filterList);
		env.writeln("List size after filter: "+filterList.size());
	}
	/**
	 * Leaves only those whose question number questionNumber corresponds with answer ans.
	 * @param env
	 * @param questionNumber
	 * @param ans
	 */
	private void filterAnswer(ExamEnvironment env, int questionNumber, String ans){
		env.writeln("List size before filter: "+filterList.size());
		filterList=filterList.stream()
				   .filter(sheet->sheet.getAnswers().get(questionNumber-1).equals(ans))
				   .collect(Collectors.toList());
		LoadCommand.setActiveSheets(filterList);
		env.writeln("List size after filter: "+filterList.size());
		
	}
	
	

}
