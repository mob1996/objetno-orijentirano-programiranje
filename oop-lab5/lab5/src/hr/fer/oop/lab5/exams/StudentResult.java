package hr.fer.oop.lab5.exams;

import java.util.Map;
import java.util.OptionalDouble;

import hr.fer.oop.lab5.shell.CommandStatus;

public class StudentResult extends ExamCommand {
    private static Map<String,SheetData> resultMap;
	
	public StudentResult() {
		super("STUDENT-RESULT");
		// TODO Auto-generated constructor stub
	}
/**
 * Prints result for one student
 */
	@Override
	public CommandStatus execute(ExamEnvironment env, String str) {
		resultMap=LoadCommand.getResultsMap();
		if(resultMap.isEmpty()){
			env.writeln("Cannot get student from empty map.");
		}
		SheetData student=resultMap.get(str);
		if(student==null){
			env.writeln("Student with that jmbag doesn't exist");
		} else if(student.getTotalScore().equals(OptionalDouble.empty())){
			env.writeln("Student with that jmbag wasn't graded.");
		} else {
			env.writeln("JMBAG: "+student.getJmbag());
			env.writeln("Exam result: "+student.getTotalScore().getAsDouble());
			for(int questionNum=0;questionNum<LoadCommand.getCorrectAnswers().get("A").length;questionNum++){
				env.write((1+questionNum)+". question: "+student.getAnswers().get(questionNum));
				AnswerStatus ansStatus=student.getAnswerScores().get(questionNum).getStatus();
				char status;
				if(ansStatus.equals(AnswerStatus.ANSWERED_CORRECT)){
					status='T';
				} else if(ansStatus.equals(AnswerStatus.ANSWERED_INCORRECT)){
					status='F';
				} else {
					status='-';
				}
				env.write(" "+status+" ("+student.getAnswerScores().get(questionNum).getScore()+")");
				env.writeln("");
			}
		}
		
		return CommandStatus.CONTINUE;
	}

}
