package hr.fer.oop.lab5.exams;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;

public class SheetData implements Comparable<SheetData> {
	private String jmbag;
	private String group;
	private List<String> answers;
	private List<AnswerScore> answerScores;
	OptionalDouble totalScore=OptionalDouble.empty();
	
	public SheetData(String jmbag,String group, List<String> answers){
		this.jmbag=jmbag;
		this.group=group;
		this.answers=answers;
	}

	public OptionalDouble getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(OptionalDouble totalScore) {
		this.totalScore = totalScore;
	}

	public String getJmbag() {
		return jmbag;
	}

	public String getGroup() {
		return group;
	}

	public List<String> getAnswers() {
		return answers;
	}
	
	public List<AnswerScore> getAnswerScores() {
		return answerScores;
	}
	
	

	public void setAnswerScores(List<AnswerScore> answerScores) {
		this.answerScores = answerScores;
	}
/**
 * Calculates SheetData's hashCode
 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((jmbag == null) ? 0 : jmbag.hashCode());
		return result;
	}
/**
 * Method determines if 2 SheetData's are same
 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SheetData other = (SheetData) obj;
		if (jmbag == null) {
			if (other.jmbag != null)
				return false;
		} else if (!jmbag.equals(other.jmbag))
			return false;
		return true;
	}

/**	
 * Method is used for comparing 2 SheetData
 */
	@Override
	public int compareTo(SheetData o) {
		if (this.getTotalScore().equals(OptionalDouble.empty())&& o.getTotalScore().equals(OptionalDouble.empty())) {
			return 0;
		} else if (!this.getTotalScore().equals(OptionalDouble.empty())&& o.getTotalScore().equals(OptionalDouble.empty())) {
			return -1;
		} else if (this.getTotalScore().equals(OptionalDouble.empty())&& !o.getTotalScore().equals(OptionalDouble.empty())) {
			return 1;
		} else {
			double d1 = this.getTotalScore().getAsDouble();
			double d2 = o.getTotalScore().getAsDouble();
			if (d1 > d2) {
				return -1;
			} else if (d1 < d2) {
				return 1;
			} else {
				return 0;
			}

		}
	}
	
	@Override
	public String toString(){
		String student;
		String group=getGroup();
		if(group.equals("BLANK")){
			group="-";
		}
		student=String.format("%s %s",getJmbag(), group);
		
		for(String ans:getAnswers()){
			if(ans.equals("BLANK")){
				student=String.format("%s %s", student,"-");
			} else {
				student=String.format("%s %s", student,ans);
			}
		}
		if(getTotalScore().equals(OptionalDouble.empty())){
			student=String.format("%s %s", student,"NOT GRADED");
		} else {
			student=String.format("%s %s", student, getTotalScore().getAsDouble());
		}
		
		
		return student;
	}
	

	
	
	

}
