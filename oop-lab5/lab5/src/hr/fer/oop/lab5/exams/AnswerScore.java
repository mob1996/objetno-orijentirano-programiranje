package hr.fer.oop.lab5.exams;

public class AnswerScore {
	double score;
	AnswerStatus status;
	
	
	public AnswerScore(){
		
	}
	
	public AnswerScore(double score, AnswerStatus status){
		this.score=score;
		this.status=status;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public AnswerStatus getStatus() {
		return status;
	}

	public void setStatus(AnswerStatus status) {
		this.status = status;
	}

	
}
