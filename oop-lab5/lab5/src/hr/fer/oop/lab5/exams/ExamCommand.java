package hr.fer.oop.lab5.exams;

import hr.fer.oop.lab5.shell.CommandStatus;
import hr.fer.oop.lab5.shell.Environment;
import hr.fer.oop.lab5.shell.ShellCommand;

public abstract class ExamCommand implements ExamShellCommand {
    private String commandName;
    
    public ExamCommand(String commandName){
    	this.commandName=commandName;
    }
	@Override
	public String getCommandName() {
		return commandName;
	}

	

}
