package hr.fer.oop.lab5.exams;


import java.util.List;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import hr.fer.oop.lab5.shell.CommandStatus;

public class StatisticsCommand extends ExamCommand {
    private static List<SheetData> statList;
	
	public StatisticsCommand() {
		super("STATISTICS");
		// TODO Auto-generated constructor stub
	}
/**
 * Method is used for printing statistics from active list
 */
	@Override
	public CommandStatus execute(ExamEnvironment env, String str) {
		statList=LoadCommand.getActiveSheets();
		if(!str.isEmpty()){
			env.writeln("Statistics doesn't have arguments");
		} else if(statList.isEmpty()){
			env.writeln("Cannot calculate statistic on empty list");
		}
		printAverage(env);
		for(int questionNum=0;questionNum<LoadCommand.getCorrectAnswers().get("A").length;questionNum++){
			printForEachQuestion(env,questionNum);
		}
		printAverageForGroup(env);
		
		
		return CommandStatus.CONTINUE;
	}
	/**
	 * Prints average score for each group
	 */
	private void printAverageForGroup(ExamEnvironment env){
		Set<String> groups=new TreeSet<>(LoadCommand.getCorrectAnswers().keySet());
		for(String g:groups){
			List<SheetData> temporaryList=statList.stream()
					.filter(sheet->!sheet.getTotalScore().equals(OptionalDouble.empty()))
					.filter(sheet->sheet.getGroup().equals(g))
					.collect(Collectors.toList());
			if(!temporaryList.isEmpty()){
				double average= temporaryList.stream()
						.mapToDouble(sheet->sheet.getTotalScore().getAsDouble())
						.average().getAsDouble();
				env.writeln("Average score of group "+g+" is: "+average);
			}
		}
	}
	/**
	 * Statistic for each question. How many students gave correct/incorrect/unaswered answers.
	 * @param env
	 * @param questionNum
	 */
	private void printForEachQuestion(ExamEnvironment env,int questionNum){
		long correctAnswers=statList.stream().filter(sheet->sheet.getTotalScore()!=OptionalDouble.empty())
				.filter(sheet->sheet.getAnswerScores().get(questionNum).getStatus().equals(AnswerStatus.ANSWERED_CORRECT))
				.count();
		long wrongAnswers=statList.stream().filter(sheet->sheet.getTotalScore()!=OptionalDouble.empty())
				.filter(sheet->sheet.getAnswerScores().get(questionNum).getStatus().equals(AnswerStatus.ANSWERED_INCORRECT))
				.count();
		long unansweredAnswers=statList.stream().filter(sheet->sheet.getTotalScore()!=OptionalDouble.empty())
				.filter(sheet->sheet.getAnswerScores().get(questionNum).getStatus().equals(AnswerStatus.UNANSWERED))
				.count();
		env.write("Question " +(questionNum+1)+". :\n"+
				  "     Answered correctly: "+ correctAnswers+
				  "\n     Answered incorrectly: "+wrongAnswers+
				  "\n     Unanswered: "+unansweredAnswers);
		env.writeln("");
		
		
	}
	/**
	 * Average score from all students in the active list
	 * @param env
	 */
	private void printAverage(ExamEnvironment env){
		double average=statList.stream().filter(sheet->sheet.getTotalScore()!=OptionalDouble.empty())
				.mapToDouble(sheet->sheet.getTotalScore().getAsDouble()).average().getAsDouble();
		env.writeln("Average from active sheets: "+average);
	}

}
