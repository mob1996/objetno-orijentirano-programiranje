package hr.fer.oop.lab2.prob4;
import java.lang.Math;

/**
 * 
 * @author Luka Mijic
 * @version 1.0
 */
public class PrimeNumbers {
/**
 * Method is used to find first n prime numbers.
 * @param args Command line is used to give us number n and needed number
 *             of arguments is one.
 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
    if(args.length!=1){
    	System.out.println("Needed number of argument is one."); 	
    } else if(Integer.parseInt(args[0])<0 && args.length==1){
    	System.out.println("Argument must not be negative.");
    } else {
    	System.out.println("You requested calculation of first "
    			+ args[0]+ " prime numbers. Here they are:");
    	int noPrimeNumbers=Integer.parseInt(args[0]);
    	int counter=0;
    	int temp=2;
    	
    	while(counter<noPrimeNumbers){
    		if(calculatePrime(temp)== true){
    			System.out.println(temp);
    			counter++;
    		}
    		temp++;
    		             }
    	            }
                }
	/**
	 * Method is used to find out if given argument number is prime number.
	 * @param number 
	 * @return true if argument is prime or false if its not.
	 */
	public static boolean calculatePrime(int number){
		boolean isPrime=true;
		
		for(int i=2;i<=Math.sqrt(number);i++){
			if(number%i==0){
				isPrime=false;
				break;
			}
		}
		return isPrime;
	}
	     }

