package hr.fer.oop.lab2.prob5;
import java.lang.Math;
/**
 * 
 * @author Luka Mijic
 * @version 1.0
 */
public class PrimeFactorization {
    /**
     * Method is used for decompostion of number n to prime number factors.
     * @param args Command line argument which represents number n. 
     *             Needed only one argument.
     */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		if(args.length!=1){
			System.out.println("Invalid input!");
		} else {
           System.out.println("You have requested decomposition of number "+args[0]
        		   +" into prime factors. Here they are: ");
           
           int number=Integer.parseInt(args[0]);
           int prime;
           int counter=0;
           
           while(number>1){
        	   prime=decomposition(number);
        	   counter++;
        	   System.out.println(counter+". "+prime);
        	   number=number/prime;
        	   
                 }
		   }
	}
	/**
	 * Method is used to find first prime number that satisfies equation number%i==0
	 * @param number Number which we want to decompose next.
	 * @return i which satisfies number%i==0.
	 */
	public static int decomposition(int number){
		int p=1;
		boolean isPrime;
		
		for(int i=2;i<=number;i++){
			isPrime=true;
			for(int j=2;j<=Math.sqrt(i);j++) {
				if(i%j==0){
					isPrime=false;
					break;
				}
			}
			if(isPrime==true && number%i==0){
				p=i;
				break;
			}
		}
		return p;
	}

}
