package hr.fer.oop.lab2.prob1;
import java.util.Scanner;

/**
 * 
 * @author Luka Mijic
 * @version 1.0
 */
public class Rectangle {
    /**
     * Method which is run when we start program. It calculates and prints
     *  area and perimeter of rectangle. Standard output is used for printing.
     *  Height and Width are given through command line arguments or they are taken
     *  through standard input.
     * @param args Command lines arguments
     *        
     */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
           if(args.length==1 || args.length>2){
        	   System.out.println("Invalid number of arguments.");
           }
           
           
      
           if(args.length==2){
        	   double width=Double.valueOf(args[0]);
        	   double height=Double.valueOf(args[1]);
        	   double area=areaMethod(width, height);
        	   double perimeter=perimeterMethod(width,height);
        	   PrintCalculation(width, height, area, perimeter);
           }
           
           
           
           
           if(args.length==0){
        	   double width=Double.valueOf(widthAndHeightScanner("width"));
        	   double height=Double.valueOf(widthAndHeightScanner("height"));
        	   double area=areaMethod(width, height);
        	   double perimeter=perimeterMethod(width,height);
        	   PrintCalculation(width, height, area, perimeter);
        	   
           }
    	
     
	}
	
	/**
     * Method used for calculating rectangle area with its width and height.
     * @param width rectangle width.
     * @param height rectangle height.
     * @return rectangle area.
     */
	
    public static double areaMethod(double width,double height){
    	return width*height;
    }
    
    /**
	 * Method used for calculating rectangle perimeter using its width and height.
	 * @param width rectangle width.
	 * @param height rectangle width.
	 * @return rectangle perimeter.
	 */
    public static double perimeterMethod(double width, double height){
    	return 2*(width+height);
    }
    /**
     * Method used for taking widht or height via Standard input.
     * @param name name is "width" or "height" depending which one we want to scan.
     *             It is used so we don't need to use seperate methods for scanning
     *             width and height.
     * @return either widht or height
     */
    public static String widthAndHeightScanner(String name){
    	Scanner input= new Scanner(System.in);
    	String WorH;
    	
        while(true){
    	   System.out.format("Please provide %s:", name);
    	   WorH=input.nextLine();
    	   WorH= WorH.trim();
    	   
    	   if(WorH.isEmpty()){
    		   System.out.println("The input must not be blank.");
    		   continue;
    	   }
    	   
    	   if(Double.parseDouble(WorH)<0){
    		   System.out.println("The "+name+" must not be negative.");
    		   continue;
    	   }
    	   break;
    	     } 
         return WorH;
    }
    /**
     * Method is used for easier printing rectangles attributes.
     * @param width rectangle width
     * @param height rectangle height
     * @param area rectangle area
     * @param perimeter rectangle perimete
     */
    public static void PrintCalculation(double width, double height, double area, double perimeter){
    	 System.out.format("You have specified a rectingle of width %.1f and height %.1f. Its area is %.1f and its perimeter is %.1f.", width, height, area, perimeter);
    }
}
