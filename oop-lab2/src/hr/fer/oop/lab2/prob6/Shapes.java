package hr.fer.oop.lab2.prob6;
/**
 * 
 * @author Luka Mijic
 * @version 1.0
 *
 */
public class Shapes {
/**
 * Method is used to draw various shapes via standard output.
 * @param args Command lines argument, they are not needed.
 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		sandWatch();
		hexagon();
		cup();
		hat();
	}
	/**
	 * Draws \        /
	 *        \______/
	 */
	public static void upwards(){
		System.out.println("\\        /\n \\______/");
		}
	/**
	 * Draws   ______
	 *        /      \
	 *       /        \
	 */
	public static  void downwards(){
		System.out.println("  ______\n /      \\\n/        \\");
	}
	/**
	 * Draws +--------+
	 */
	public static void plusesAndMinuses(){
		System.out.println("+--------+");
	}
	/**
	 * Draws Sandwatch
	 *       +--------+
	 *       \        /
	 *        \______/
	 *         ______
	 *        /      \
	 *       /        \
	 *       +--------+
	 */
	public static void sandWatch(){
		plusesAndMinuses();
        upwards();
        downwards();
        plusesAndMinuses();	
	}
	/** 
	 * Draws Hexagon
	 *         ______
	 *        /      \
	 *       /        \
	 *       \        /
	 *        \______/
	 */
	public static void hexagon(){
		downwards();
		upwards();
		System.out.println("");
	}
	/** 
	 * Draws Cup
	 *       \        /
	 *        \______/
	 *       +--------+
	 */
	public static void cup(){
		upwards();
		plusesAndMinuses();
	}
	/**
	 * Draws hat
	 *         ______
	 *        /      \
	 *       /        \
	 *       +--------+
	 */
	public static void hat(){
		downwards();
		plusesAndMinuses();
	}

}
