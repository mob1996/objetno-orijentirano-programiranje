package hr.fer.oop.lab2.prob3;
import java.lang.Math;

/**
 * @author Luka Mijic
 * @version 1.0
 */

public class Roots {
	/**
	 * Method is used to calculate and print n-th root of complex number. 
	 * Example of complex number z=x+yi. 
	 * @param args x, y and n are given via command line.
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
    System.out.println("You have entered complex number z = "+args[0]+" + "+args[1]+"i and you"
    		+ " have requested calculation of "+args[2]+". roots.");
    
     double x=Double.parseDouble(args[0]);
     double y=Double.parseDouble(args[1]);
     
     /*
      * We are changing z=x+yi to trigonometry shape.
      * z=r(cos(fi)+i*sin(fi) where r=sqrt(x^2,y^2) and fi=atan(y/x);
      */
     
     double root=Double.parseDouble(args[2]);
     double r=Math.hypot(x, y);
     double fi=Math.atan(y/x);
     
     rootsCalculator(r, fi, root);
	}
	/**
	 * Method is used to calculate and print n-th root of complex number using trigonomety shape.
	 * pow(z,1/n)=pow(r,1/n)*(cos((fi+2kPI)/n)+isin((fi+2kPI)/n))
	 * After calculating n-th root its turning trigonomety shape to ordinary shape using
	 * formula x(k)=r*cos((fi+2kPI)/n and y(k)=sin((fi+2kPI)/n) and k=0,1....(n-1).
	 * @param r
	 * @param fi
	 * @param root
	 */
     public static  void rootsCalculator (double r, double fi, double root){
    	double rootn=Math.pow(r, 1/root);
    	double newX;
    	double newY;
    	
    	for(int i=0;i<root;i++) {
    		newX=rootn*Math.cos((fi+2*i*Math.PI)/root);
    		newY=rootn*Math.sin((fi+2*i*Math.PI)/root);
    		System.out.format("%d) %.2f + %.2fi\n",(i+1), newX, newY);
    	}
     }
	
	
	
}
