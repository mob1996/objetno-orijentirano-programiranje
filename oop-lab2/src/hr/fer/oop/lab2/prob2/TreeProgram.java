package hr.fer.oop.lab2.prob2;
/**
 * 
 * @author Luka Mijic
 * @version 1.0
 */

public class TreeProgram {
	
	static class TreeNode {
		TreeNode left;
		TreeNode right;
		String data;
	}
	/**
	 * Method is used when we start program.
	 * @param args Command line arguments, program doesn't use them.
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TreeNode node=null;
		
		node=insert(node,"Jasna");
		node=insert(node,"Ana");
		node=insert(node,"Ivana");
		node=insert(node,"Anamarija");
		node=insert(node,"Vesna");		
		node=insert(node, "Kristina");
		
		System.out.println("Writing tree inorder: ");
		writeTree(node);
		
		node=reverseTreeOrder(node);
		
		System.out.println("\nWriting reversed tree inorder:");
		writeTree(node);
		
		int size= sizeOfTree(node);
		System.out.println("\nNumber of nodes in tree is "+size+".");
		
		boolean found=containsData(node,"Ivana");
		System.out.println("Searched element is found: "+found);

	}
	/**
	 * Method is used when we want to find out if data is part of the Tree.
	 * @param treeRoot Tree root.
	 * @param data information that we want to find in the tree.
	 * @return true if we find data or false if we dont find data in the tree.
	 */
	static boolean containsData(TreeNode treeRoot, String data){
		if(treeRoot==null)
			return false;
		if(data.equals(treeRoot.data))
			return true;
		
		boolean leftKid;
		boolean rightKid;
		
		if(treeRoot.left!=null)
			leftKid=containsData(treeRoot.left, data);
		else
			leftKid= false;
		
		if(treeRoot.right!=null)
			rightKid=containsData(treeRoot.right,data);
		else
			rightKid= false;
		
		return rightKid || leftKid;
	}
	/**
	 * Method which is used to find out how many nodes are in the tree.
	 * @param treeRoot Tree root.
	 * @return number of nodes
	 */
	static int sizeOfTree(TreeNode treeRoot){
		if(treeRoot==null)
			return 0;
		int left;
		int right;
		
		if(treeRoot.left!=null)
			left=sizeOfTree(treeRoot.left);
		else
			left=0;
		
		if(treeRoot.right!=null)
			right=sizeOfTree(treeRoot.right);
		else
			right=0;
		
		return right+left+1;
		}
	
	/**
	 * Method is used to inserts data in its appropriate position in the tree.
	 * @param treeRoot Tree root.
	 * @param data Information that we are inserting into the tree.
	 * @return new tree node.
	 */
	static TreeNode insert (TreeNode treeRoot, String data){
		if(treeRoot==null){
			treeRoot=new TreeNode();
			treeRoot.right=treeRoot.left=null;
			treeRoot.data=data;
			return treeRoot;
			
		} else {
			if(treeRoot.data.compareTo(data) > 0)
				treeRoot.left=insert(treeRoot.left, data);
			
			else
				treeRoot.right=insert(treeRoot.right, data);
			
			return treeRoot;
		}
		
	}
	/**
	 * Method is used to print all data in the tree with inorder method.
	 * @param treeRoot Tree root.
	 */
	static void writeTree(TreeNode treeRoot){
		if(treeRoot==null)
			return;
		
		writeTree(treeRoot.left);		
		System.out.format("%s, ", treeRoot.data);
		writeTree(treeRoot.right);
	}
	
	/**
	 * Method used to reverse tree.
	 * @param treeRoot Tree root.
	 * @return root of the reversed tree.
	 */
	static TreeNode reverseTreeOrder(TreeNode treeRoot){
		if(treeRoot==null)
			return treeRoot;
		
		TreeNode helpNode=treeRoot.right;
		treeRoot.right=treeRoot.left;
		treeRoot.left=helpNode;
		
		if (treeRoot.left == null)
			reverseTreeOrder(treeRoot.left);
		if (treeRoot.right == null)
			reverseTreeOrder(treeRoot.right);

		return treeRoot;
	}

}
