package hr.fer.oop.lab3.prob1;
import hr.fer.oop.lab3.pic.Picture;
/**
 * Class rectangle contains methods for defining and drawing rectangle on the
 * given Picture using length of width, height and a staring point, or
 * by using predefined rectangle
 * @author Luka  Mijic
 *
 */
public class Rectangle implements IDrawOnPicture {
      private int width;
      private int height;
      private Point start;
      
      /**
       * Constructor used for defining rectangle using width, lenght and starting point
       * @param width rectangle widht
       * @param height rectangle height
       * @param start rectangles starting point
       */
      public Rectangle(int width, int height, Point start){
    	  this.width=width;
    	  this.height=height;
    	  this.start=start;
      }
      /**
       * Constructor used for defining rectangle using predefined 
       * rectangle
       * @param rectangle
       */
     public Rectangle(Rectangle rectangle){
    	 this(rectangle.getWidth(), rectangle.getHeight(), rectangle.start);
     }

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Point getPoint() {
		return start;
	}

	public void setPoint(Point Point) {
		this.start = Point;
	}
	/**
	 * Method used for drawing rectangle on the given picture
	 * by turning on pixels that are part of the given rectangle.
	 * @param pic Picuture we are are drawing on.
	 * 
	 */
    @Override
    public void drawOnPicture(Picture pic){
    	for(int i=start.getX();i<start.getX()+getWidth();i++){
    		for(int j=start.getY();j<start.getY()+getHeight();j++){
    			if(i>=0 && i<pic.getWidth()&& j>=0 && j<pic.getHeight())
    			pic.turnPixelOn(i, j);
    			
    		}
    		
    	}
    }
}
