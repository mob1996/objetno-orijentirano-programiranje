package hr.fer.oop.lab3.prob1;
import hr.fer.oop.lab3.pic.Picture;
import hr.fer.oop.lab3.pic.PictureDisplay;
/**
 * Class EquilateralTriangle contains methods for defining
 * and drawing equilateral triangle on give picture.
 * @author Luka Mijic
 *
 */
public class EquilateralTriangle implements IDrawOnPicture {
	private int a;
	private Point start;
	/**
	 * Constructor used for defining equilateral triangle.
	 * @param a is lenght of the sides of the triangle
	 * @param start Point from where we start drawing triangle
	 */
	public EquilateralTriangle(int a, Point start){
		this.a=a;
		this.start=start;
	}
	/**
	 * Method used for defining triangle using predefined triangle.
	 * @param triangle1 argument used for shaping new triangle.
	 */
	public EquilateralTriangle(EquilateralTriangle triangle1){
		this(triangle1.a, triangle1.start);
	}
	public int getA() {
		return a;
	}
	public void setA(int a) {
		this.a = a;
	}
	/**
	 * Method is used to calculate equilateral triangle height using
	 * formula a*sqrt(3)/2. Since we cant have 0.something of a pixel,
	 * we are rounding the height to the closest whole number.
	 * @return triangle height
	 */
	public int triangleHeight(){
		double heightDouble=getA()*Math.sqrt(3)/2.;
		int height=(int)heightDouble;
		if(heightDouble%height>=0.5)
			return height+1;
		return height;
	}
	public Point getStart() {
		return start;
	}
	public void setStart(Point start) {
		this.start = start;
	}
/**
 * Method is used for drawing equilateral triangle on given Picure.
 * Algorithm behind this methos is based on the fact that all 3 
 * angles in the equilateral triangle are 60 degrees.
 * 
 */
	public void drawOnPicture(Picture pic){
		
		for(int i=start.getX();i<=start.getX()+getA();i++){
			for(int j=start.getY();j>=start.getY()-triangleHeight();j--){
				double lenghtX1=i-start.getX();
				double lenghtX2=start.getX()+getA()-i;
				double lenghtY=start.getY()-j;
				double mathWizard1=lenghtY/lenghtX1;
				double mathWizard2=lenghtY/lenghtX2;
				double angle1=Math.atan(mathWizard1);
				double angle2=Math.atan(mathWizard2);
				if(i>=0 && i<pic.getWidth()&& 
				   j>=0 && j<pic.getHeight() && 
				   angle1<=Math.PI/3.0 && angle2<=Math.PI/3.0 && 
				   angle1>=0 && angle2>=0)
					pic.turnPixelOn(i, j);
			}
			
				
		}
		        
		
	}

}
