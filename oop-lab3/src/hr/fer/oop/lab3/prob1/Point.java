package hr.fer.oop.lab3.prob1;
/**
 * Class Point is used for the defining 
 * starting point of equilateral triangle and rectangle
 * or for the centre of the circle
 * @author Luka Mijic
 *
 */
public class Point {

	private int x;
	private int y;
	
	public Point(int x, int y){
		this.x=x;
		this.y=y;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	@Override
	public String toString(){
		return String.format("(%d,%d)", x, y);
	}


}
