package hr.fer.oop.lab3.prob1;
import hr.fer.oop.lab3.pic.Picture;
import hr.fer.oop.lab3.pic.PictureDisplay;

/**
 * Method thats prints 6 diffrent shapes on picture.
 * @author Luka Mijic
 *
 */
public class Demonstration {
	public static void main(String[] args){
	
	   Picture pic=new Picture(100, 50);
	   Circle circle=new Circle(10,new Point(20,30));
	   Circle circle2=new Circle(circle);
	   Circle circle3=new Circle(10, new Point(95,45));
	   Rectangle rectangle=new Rectangle (10, 5, new Point(3,40));
	   Rectangle rectangle2=new Rectangle(rectangle);
	   Rectangle rectangle3=new Rectangle(5,4, new Point(50,20));
	   EquilateralTriangle triangle= new EquilateralTriangle(10,new Point(70,30));
	   EquilateralTriangle triangle2= new EquilateralTriangle(triangle);
	   EquilateralTriangle triangle3= new EquilateralTriangle(15, new Point(4, 5));
	   
	   
	  
	  rectangle2.drawOnPicture(pic);
	  rectangle3.drawOnPicture(pic);
	  circle2.drawOnPicture(pic);
	  circle3.drawOnPicture(pic);
	  triangle2.drawOnPicture(pic);
	  triangle3.drawOnPicture(pic);
	  
	  
	   pic.renderImageToStream(System.out);
	   PictureDisplay.showPicture(pic,10);
	   


	


	}

}
