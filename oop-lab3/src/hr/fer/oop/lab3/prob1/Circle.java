package hr.fer.oop.lab3.prob1;
import hr.fer.oop.lab3.pic.Picture;
import hr.fer.oop.lab3.pic.PictureDisplay;

/**
 * Class Circle contains method for defining new Circle using
 * cordinates that are given via constructors or using already defined
 * circle. It also contains drawOnPicture method that is used to
 * turn pixels in shape of a circle.
 * @author Luka Mijic
 *
 */
public class Circle implements IDrawOnPicture {
       private int radius;
       private Point center;
       /**
        * Method used for creating new circle with radius and center point
        * @param radius Radius of circle
        * @param center is instance of class Point and it contains 
        *        x and y axis of center point
        */
     public Circle(int radius, Point center){
    	   this.radius=radius;
    	   this.center=center;
       }
     /**
      * Method used for creating new circle using already defined circle
      * @param circle already defined circle
      */
     public Circle(Circle circle){
    	 this(circle.getRadius(), circle.getCenter());
     }

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}
    
	public Point getCenter() {
		return center;
	}

	public void setPoint(Point Point) {
		this.center = Point;
	}
	
	/**
	 * Method which calculates what pixels are part of the needed circle.
	 * If pixel is part of the circle it turns him on.
	 * @param p Picture on which we are drawing circle
	 */
	@Override
	public void drawOnPicture(Picture pic){
		for(int i=center.getX()-getRadius();i<=center.getX()+getRadius();i++){
			for(int j=center.getY()-getRadius();j<=center.getY()+getRadius();j++){
				if(i>=0 && i<pic.getWidth()&&
				   j>=0 && j<pic.getHeight() &&
				   Math.sqrt(Math.pow(i-center.getX(), 2)+Math.pow(j-center.getY(), 2))<=getRadius()){
				   pic.turnPixelOn(i, j);
				}
			}
		}
	}
      
       
}
