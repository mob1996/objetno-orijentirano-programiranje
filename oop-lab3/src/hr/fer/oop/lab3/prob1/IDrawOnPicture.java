package hr.fer.oop.lab3.prob1;
import hr.fer.oop.lab3.pic.Picture;
import hr.fer.oop.lab3.pic.PictureDisplay;
public interface IDrawOnPicture {
public void drawOnPicture(Picture pic);
}
