package hr.fer.oop.lab3.prob2;
/**
 * Class SimpleHashtable is contains methods
 * for creating new hash table
 * @author Luka Mijic
 *
 * @param <K> key 
 * @param <V> value
 */
public class SimpleHashtable<K, V> { 
	private int size;
	private TableEntry<K, V>[] table;
	/**
	 * default constructor
	 * sets size on 16
	 */
	public SimpleHashtable(){
		this.size=16;
		this.table=new TableEntry[size];
	}
	/**
	 * Constructor used for setting size to closest
	 * power of two
	 * @param size of the table
	 */
	public SimpleHashtable(int size){
		this.size=calculateSize(size);
		
		this.table=new TableEntry[this.size];
	}
/**
 * Method which is used to calculate closest
 * power of 2 to size
 * @param size
 * @return
 */
	public int calculateSize(int size){
		int power=2;
		while(power<size){
			power*=2;
		}
		return power;
	}
	public int getSize() {
		return size;
	}
	/**
	 * Method is used for placing new object in to the HashTable.
	 * If given key was already placed into the table, it updates its value.
	 * @param key 
	 * @param value
	 */
	public void put(K key, V value){
		int slot=Math.abs(key.hashCode())%size;
		TableEntry tableTraversal=table[slot];
		if(tableTraversal==null){
			TableEntry<K,V> newEntry= new TableEntry<>(key, value,null);
			table[slot]=newEntry;
		} else {
			while(tableTraversal!=null){
				if(tableTraversal.key.equals(key)){
					tableTraversal.setValue(value);
					break;
				}else if(tableTraversal.next==null){
					TableEntry<K, V> newEntry= new TableEntry<>(key,value, null);
					tableTraversal.next=newEntry;
					break;
				}
				tableTraversal=tableTraversal.next;
			}
		}
	}
	/**
	 * Method is used to find given key and return its value.
	 * @param key
	 * @return value
	 */
	public V get(K key){
		int slot=Math.abs(key.hashCode())%size;
		TableEntry<K, V> tableTraversal=table[slot];
		while(tableTraversal!=null){
			if(tableTraversal.key.equals(key))
				return tableTraversal.getValue();
			tableTraversal=tableTraversal.next;
		}
		return null;
	}
	/**
	 * Method is used to calculate how many elements are in the
	 * HashTable
	 * @return number of elements
	 */
	public int size(){
		int numberOfKeys=0;
		for(int i=0;i<size;i++){
			TableEntry<K, V> tableTraversal=table[i];
			while(tableTraversal!=null){
				numberOfKeys++;
				tableTraversal=tableTraversal.next;
				}
			
		}
		return numberOfKeys;
	}
	/**
	 * Method is used to check if HashTable contains given key.
	 * @param key
	 * @return true if it contains given key, false if it does not
	 */
	public boolean containsKey(K key){
		int slot=Math.abs(key.hashCode())%size;
		TableEntry<K, V> tableTraversal=table[slot];
		while(tableTraversal!=null){
			if(tableTraversal.getKey().equals(key))
				return true;
			tableTraversal=tableTraversal.next;
		}
		return false;
	}
	/**
	 * Method is used to check if HashTable contains given value.
	 * @param value
	 * @return true if it contains given value, false if it does not.
	 */
	public boolean containsValue(V value){
		for(int i=0;i<size;i++){
			TableEntry<K, V> slotTraversal=table[i];
			while(slotTraversal!=null){
				if(slotTraversal.getValue().equals(value))
					return true;
				slotTraversal=slotTraversal.next;
			}
		}
		return false;
	}
	/**
	 * Method is used for removing element whose key is the 
	 * same as given key.
	 * @param key
	 */
	public void remove(K key){
      if(containsKey(key)){
		int slot=Math.abs(key.hashCode())%size;
		TableEntry<K, V> tableTraversal=table[slot];
		if(tableTraversal.getKey().equals(key)){
			table[slot]=tableTraversal.next;
		    return;
		}
		while(tableTraversal!=null){
			if(tableTraversal.next.getKey().equals(key)){
				tableTraversal.next=tableTraversal.next.next;
				break;
			}
			tableTraversal=tableTraversal.next;
		}
	} else
		 System.out.println("There is no student with name: "+key);		
		    
	}
	/**
	 * Method is used to check if HashTable is empty
	 * @return true if it is empty, false if it is not
	 */
	public boolean isEmpty(){
		for(int i=0;i<size;i++){
			if(table[i]!=null)
				return false;
		}
		return true;
	}
	
	/**
	 * Method is used to turn HashTable into a String
	 */
	@Override
	public String toString(){
		String output="";
		for(int i=0;i<size;i++){
			TableEntry<K, V> tableTraversal=table[i];
			while(tableTraversal!=null){
				output+=tableTraversal+"  ";
				tableTraversal=tableTraversal.next;
			}
			output+="\n";
		}
		System.out.println("Grading table");
		return output;
	}
	
	





/**
 * TableEntry is data structure which contains key and value
 * and a reference to next TableEntry. It is used to create lists.
 * @author Luka Mijic
 *
 * @param <K> key
 * @param <V> value
 */


	private static class TableEntry<K, V>{
		private K key;
		private V value;
		private TableEntry<K,V> next;
		
		/**
		 * Constructor used for creating TableEntry
		 * @param key TableEntry key
		 * @param value TableEntry value
		 * @param next TableEntry next
		 */
		public TableEntry(K key, V value, TableEntry<K, V> next){
			this.key=key;
			this.value=value;
			this.next=next;
		}

		public V getValue() {
			return value;
		}

		public void setValue(V value) {
			this.value = value;
		}

		public K getKey() {
			return key;
		}
		/**
		 * Method used for turning TableEntry into string
		 * formatted to look like (key,value)
		 */
		@Override
		public String toString(){
			return "("+key+", Grade: "+value+")";
		}
	}
	

}
