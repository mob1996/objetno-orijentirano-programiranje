package hr.fer.oop.lab3.prob2;
/**
 * Class that is used to show that SimpleHashtable works.
 * @author Luka Mijic
 *
 */
public class Demonstration {

	public static void main(String[] args) {
		SimpleHashtable<String, Integer> examMarks=new SimpleHashtable<>(2);
		System.out.println("Grading table is empty: "+examMarks.isEmpty() );
		
		examMarks.put("Luka", 3);
		examMarks.put("Lovro", 5);
		examMarks.put("Marko", 5);
		examMarks.put("Luka", 5);
		examMarks.put("Dora", 4);
		examMarks.put("Ivana", 3);
		examMarks.put("Davor", 3);
		examMarks.put("Kreso", 1);
		examMarks.put("Ivan", 1);
		examMarks.put("Lucija", 4);
		
		System.out.println("Luka recieved grade: "+examMarks.get("Luka"));
		System.out.println("Ivana recieved grade: "+examMarks.get("Ivana"));
		System.out.println("Dora recieved grade: "+examMarks.get("Dora"));
		System.out.println("Lucija recieved grade: "+examMarks.get("Lucija"));
		
		System.out.println("Number of people who took the test: "+ examMarks.size());
		examMarks.remove("Kreso");
		examMarks.remove("Ivan");
		examMarks.remove("Dalibor");
		System.out.println("Number of people who passed the test: "+ examMarks.size());
		
		
		System.out.println("Did anyone get 5: "+examMarks.containsValue(5));
		System.out.println("Did anyone get 2: "+examMarks.containsValue(2));
		
		System.out.println("Did Luka took the test: "+examMarks.containsKey("Luka"));
		System.out.println("Did Filip took the test: "+examMarks.containsKey("Filip"));
		
		System.out.println("Is grading table empty?:" + examMarks.isEmpty());
		System.out.println(examMarks);
		
		
		
		
		
		

	}

}
