package hr.fer.oop.lab3.prob3;
/**
 * Class Demonstration is used to check if program works.
 * @author Luka Mijic
 *
 */
public class Demonstration {

	public static void main(String[] args) {
		SimpleHashtable<String,Integer> examMarks; 
	    examMarks = new SimpleHashtable<>(2); 
	    
	    examMarks.put("Ivana", 2);
	    examMarks.put("Ante", Integer.valueOf(2));
	    examMarks.put("Jasna", Integer.valueOf(2));
	    examMarks.put("Kristina", Integer.valueOf(5));
	    examMarks.put("Ivana", Integer.valueOf(5)); // overwrites old grade for Ivana
	  

	   
	    System.out.println("Prvi obilazak kroz tablicu");  
	    for(SimpleHashtable.TableEntry<String,Integer> entry : examMarks) {   
	       System.out.printf("%s => %s%n", entry.getKey(), entry.getValue()); 
	    } 
	    System.out.println("Drugi obilazak kroz tablicu");  
	    for(SimpleHashtable.TableEntry<String,Integer> entry : examMarks) {   
	        System.out.printf("%s => %s%n", entry.getKey(), entry.getValue()); 
		
		
		
		
		
		

	}

}
}
